<?php

/**
* classe Tag_pergunta
*
* @author Instalador LazyPHP <http://lazyphp.com.br>
* @version 26/10/2018 08:59
*/
final class Tag_pergunta extends Record{ 

    const TABLE = 'tag_pergunta';
    const PK = 'id_Tag_Pergunta';
    
    public $id_Tag_Pergunta;
    public $id_Tag;
    public $id_Discursiva;
    public $id_Objetiva;
    public $id_Somatoria;
    
    /**
    * Configurações e filtros globais do modelo
    * @return Criteria $criteria
    */
    public static function configure(){
        $criteria = new Criteria();
        $criteria->paginate(20, 'paginaTag_pergunta');
        return $criteria;
    }
    
    /**
    * Sanitize - filtra os caracteres válidos para cada atributo
    * Configure corretamente por questão de segurança (XSS)
    * Este método é chamado automaticamente pelo método save() da superclasse
    */
    public function sanitize(){
            $this->id_Tag_Pergunta = 	filter_var($this->id_Tag_Pergunta, FILTER_SANITIZE_NUMBER_INT);
            $this->id_Tag = 	filter_var($this->id_Tag, FILTER_SANITIZE_NUMBER_INT);
            $this->id_Discursiva = 	filter_var($this->id_Discursiva, FILTER_SANITIZE_NUMBER_INT);
            $this->id_Objetiva = 	filter_var($this->id_Objetiva, FILTER_SANITIZE_NUMBER_INT);
            $this->id_Somatoria = 	filter_var($this->id_Somatoria, FILTER_SANITIZE_NUMBER_INT);
    }
    
    /**
    * Tag_pergunta pertence a Discursiva
    * @return Discursiva $Discursiva
    */
    function getDiscursiva() {
        return $this->belongsTo('Discursiva','id_Discursiva');
    }
    
    /**
    * Tag_pergunta pertence a Objetiva
    * @return Objetiva $Objetiva
    */
    function getObjetiva() {
        return $this->belongsTo('Objetiva','id_Objetiva');
    }
    
    /**
    * Tag_pergunta pertence a Somatoria
    * @return Somatoria $Somatoria
    */
    function getSomatoria() {
        return $this->belongsTo('Somatoria','id_Somatoria');
    }
    
    /**
    * Tag_pergunta pertence a Tag
    * @return Tag $Tag
    */
    function getTag() {
        return $this->belongsTo('Tag','id_Tag');
    }
}