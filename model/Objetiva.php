<?php

/**
* classe Objetiva
*
* @author Instalador LazyPHP <http://lazyphp.com.br>
* @version 26/10/2018 08:59
*/
final class Objetiva extends Record{ 

    const TABLE = 'objetiva';
    const PK = 'id_Objetiva';
    
    public $id_Objetiva;
    public $texto;
    public $checking;
    public $dificuldade;
    public $id_usuario;
    
    /**
    * Configurações e filtros globais do modelo
    * @return Criteria $criteria
    */
    public static function configure(){
        $criteria = new Criteria();
        $criteria->paginate(20, 'paginaObjetiva');
        return $criteria;
    }
    
    /**
    * Sanitize - filtra os caracteres válidos para cada atributo
    * Configure corretamente por questão de segurança (XSS)
    * Este método é chamado automaticamente pelo método save() da superclasse
    */
    public function sanitize(){
            $this->id_Objetiva = 	filter_var($this->id_Objetiva, FILTER_SANITIZE_NUMBER_INT);
            $this->texto = 	$this->htmlFilter($this->texto); # vide /lib/core/Record.php
            $this->checking = 	filter_var($this->checking, FILTER_SANITIZE_NUMBER_INT);
            $this->dificuldade = 	filter_var($this->dificuldade, FILTER_SANITIZE_NUMBER_INT);
            $this->id_usuario = 	filter_var($this->id_usuario, FILTER_SANITIZE_NUMBER_INT);
    }
    
    /**
    * Objetiva pertence a Usuario
    * @return Usuario $Usuario
    */
    function getUsuario() {
        return $this->belongsTo('Usuario','id_usuario');
    }
    
    /**
    * Objetiva possui Respostas
    * @return Resposta[] array de Respostas
    */
    function getRespostas( $criteria = NULL ) {
        return $this->hasMany('Resposta','id_Objetiva',$criteria);
    }
    
    /**
    * Objetiva possui Discursivas via Resposta (NxN)
    * @return Discursiva[] array de Discursivas
    */
    function getRespostaDiscursivas( $criteria = NULL ) {
        return $this->hasNN('Resposta','id_Objetiva','id_Discursiva','Discursiva',$criteria);
    }
    
    /**
    * Objetiva possui Somatorias via Resposta (NxN)
    * @return Somatoria[] array de Somatorias
    */
    function getRespostaSomatorias( $criteria = NULL ) {
        return $this->hasNN('Resposta','id_Objetiva','id_Somatoria','Somatoria',$criteria);
    }
    
    /**
    * Objetiva possui Tag_perguntas
    * @return Tag_pergunta[] array de Tag_perguntas
    */
    function getTag_perguntas( $criteria = NULL ) {
        return $this->hasMany('Tag_pergunta','id_Objetiva',$criteria);
    }
    
    /**
    * Objetiva possui Discursivas via Tag_pergunta (NxN)
    * @return Discursiva[] array de Discursivas
    */
    function getTag_perguntaDiscursivas( $criteria = NULL ) {
        return $this->hasNN('Tag_pergunta','id_Objetiva','id_Discursiva','Discursiva',$criteria);
    }
    
    /**
    * Objetiva possui Somatorias via Tag_pergunta (NxN)
    * @return Somatoria[] array de Somatorias
    */
    function getTag_perguntaSomatorias( $criteria = NULL ) {
        return $this->hasNN('Tag_pergunta','id_Objetiva','id_Somatoria','Somatoria',$criteria);
    }
    
    /**
    * Objetiva possui Tags via Tag_pergunta (NxN)
    * @return Tag[] array de Tags
    */
    function getTag_perguntaTags( $criteria = NULL ) {
        return $this->hasNN('Tag_pergunta','id_Objetiva','id_Tag','Tag',$criteria);
    }
}