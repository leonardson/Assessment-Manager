<?php

/**
* classe Discursiva
*
* @author Instalador LazyPHP <http://lazyphp.com.br>
* @version 26/10/2018 08:59
*/
final class Discursiva extends Record{ 

    const TABLE = 'discursiva';
    const PK = 'id_Discursiva';
    
    public $id_Discursiva;
    public $texto;
    public $dificuldade;
    public $id_usuario;
    
    /**
    * Configurações e filtros globais do modelo
    * @return Criteria $criteria
    */
    public static function configure(){
        $criteria = new Criteria();
        $criteria->paginate(20, 'paginaDiscursiva');
        return $criteria;
    }
    
    /**
    * Sanitize - filtra os caracteres válidos para cada atributo
    * Configure corretamente por questão de segurança (XSS)
    * Este método é chamado automaticamente pelo método save() da superclasse
    */
    public function sanitize(){
            $this->id_Discursiva = 	filter_var($this->id_Discursiva, FILTER_SANITIZE_NUMBER_INT);
            $this->texto = 	$this->htmlFilter($this->texto); # vide /lib/core/Record.php
            $this->dificuldade = 	filter_var($this->dificuldade, FILTER_SANITIZE_NUMBER_INT);
            $this->id_usuario = 	filter_var($this->id_usuario, FILTER_SANITIZE_NUMBER_INT);
    }
    
    /**
    * Discursiva pertence a Usuario
    * @return Usuario $Usuario
    */
    function getUsuario() {
        return $this->belongsTo('Usuario','id_usuario');
    }
    
    /**
    * Discursiva possui Respostas
    * @return Resposta[] array de Respostas
    */
    function getRespostas( $criteria = NULL ) {
        return $this->hasMany('Resposta','id_Discursiva',$criteria);
    }
    
    /**
    * Discursiva possui Objetivas via Resposta (NxN)
    * @return Objetiva[] array de Objetivas
    */
    function getRespostaObjetivas( $criteria = NULL ) {
        return $this->hasNN('Resposta','id_Discursiva','id_Objetiva','Objetiva',$criteria);
    }
    
    /**
    * Discursiva possui Somatorias via Resposta (NxN)
    * @return Somatoria[] array de Somatorias
    */
    function getRespostaSomatorias( $criteria = NULL ) {
        return $this->hasNN('Resposta','id_Discursiva','id_Somatoria','Somatoria',$criteria);
    }
    
    /**
    * Discursiva possui Tag_perguntas
    * @return Tag_pergunta[] array de Tag_perguntas
    */
    function getTag_perguntas( $criteria = NULL ) {
        return $this->hasMany('Tag_pergunta','id_Discursiva',$criteria);
    }
    
    /**
    * Discursiva possui Objetivas via Tag_pergunta (NxN)
    * @return Objetiva[] array de Objetivas
    */
    function getTag_perguntaObjetivas( $criteria = NULL ) {
        return $this->hasNN('Tag_pergunta','id_Discursiva','id_Objetiva','Objetiva',$criteria);
    }
    
    /**
    * Discursiva possui Somatorias via Tag_pergunta (NxN)
    * @return Somatoria[] array de Somatorias
    */
    function getTag_perguntaSomatorias( $criteria = NULL ) {
        return $this->hasNN('Tag_pergunta','id_Discursiva','id_Somatoria','Somatoria',$criteria);
    }
    
    /**
    * Discursiva possui Tags via Tag_pergunta (NxN)
    * @return Tag[] array de Tags
    */
    function getTag_perguntaTags( $criteria = NULL ) {
        return $this->hasNN('Tag_pergunta','id_Discursiva','id_Tag','Tag',$criteria);
    }
}