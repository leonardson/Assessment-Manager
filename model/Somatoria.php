<?php

/**
* classe Somatoria
*
* @author Instalador LazyPHP <http://lazyphp.com.br>
* @version 26/10/2018 08:59
*/
final class Somatoria extends Record{ 

    const TABLE = 'somatoria';
    const PK = 'id_Somatoria';
    
    public $id_Somatoria;
    public $texto;
    public $checking;
    public $dificuldade;
    public $id_usuario;
    
    /**
    * Configurações e filtros globais do modelo
    * @return Criteria $criteria
    */
    public static function configure(){
        $criteria = new Criteria();
        $criteria->paginate(20, 'paginaSomatoria');
        return $criteria;
    }
    
    /**
    * Sanitize - filtra os caracteres válidos para cada atributo
    * Configure corretamente por questão de segurança (XSS)
    * Este método é chamado automaticamente pelo método save() da superclasse
    */
    public function sanitize(){
            $this->id_Somatoria = 	filter_var($this->id_Somatoria, FILTER_SANITIZE_NUMBER_INT);
            $this->texto = 	$this->htmlFilter($this->texto); # vide /lib/core/Record.php
            $this->checking = 	filter_var($this->checking, FILTER_SANITIZE_NUMBER_INT);
            $this->dificuldade = 	filter_var($this->dificuldade, FILTER_SANITIZE_NUMBER_INT);
            $this->id_usuario = 	filter_var($this->id_usuario, FILTER_SANITIZE_NUMBER_INT);
    }
    
    /**
    * Somatoria possui Respostas
    * @return Resposta[] array de Respostas
    */
    function getRespostas( $criteria = NULL ) {
        return $this->hasMany('Resposta','id_Somatoria',$criteria);
    }
    
    /**
    * Somatoria possui Discursivas via Resposta (NxN)
    * @return Discursiva[] array de Discursivas
    */
    function getRespostaDiscursivas( $criteria = NULL ) {
        return $this->hasNN('Resposta','id_Somatoria','id_Discursiva','Discursiva',$criteria);
    }
    
    /**
    * Somatoria possui Objetivas via Resposta (NxN)
    * @return Objetiva[] array de Objetivas
    */
    function getRespostaObjetivas( $criteria = NULL ) {
        return $this->hasNN('Resposta','id_Somatoria','id_Objetiva','Objetiva',$criteria);
    }
    
    /**
    * Somatoria pertence a Usuario
    * @return Usuario $Usuario
    */
    function getUsuario() {
        return $this->belongsTo('Usuario','id_usuario');
    }
    
    /**
    * Somatoria possui Tag_perguntas
    * @return Tag_pergunta[] array de Tag_perguntas
    */
    function getTag_perguntas( $criteria = NULL ) {
        return $this->hasMany('Tag_pergunta','id_Somatoria',$criteria);
    }
    
    /**
    * Somatoria possui Discursivas via Tag_pergunta (NxN)
    * @return Discursiva[] array de Discursivas
    */
    function getTag_perguntaDiscursivas( $criteria = NULL ) {
        return $this->hasNN('Tag_pergunta','id_Somatoria','id_Discursiva','Discursiva',$criteria);
    }
    
    /**
    * Somatoria possui Objetivas via Tag_pergunta (NxN)
    * @return Objetiva[] array de Objetivas
    */
    function getTag_perguntaObjetivas( $criteria = NULL ) {
        return $this->hasNN('Tag_pergunta','id_Somatoria','id_Objetiva','Objetiva',$criteria);
    }
    
    /**
    * Somatoria possui Tags via Tag_pergunta (NxN)
    * @return Tag[] array de Tags
    */
    function getTag_perguntaTags( $criteria = NULL ) {
        return $this->hasNN('Tag_pergunta','id_Somatoria','id_Tag','Tag',$criteria);
    }
}