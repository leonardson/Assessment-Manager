<?php

/**
* classe Questao
*
* @author Instalador LazyPHP <http://lazyphp.com.br>
* @version 17/10/2018 19:46
*/
final class Questao extends Record{ 

    const TABLE = 'questao';
    const PK = 'id_Questao';
    
    public $id_Questao;
    public $enunciado;
    public $foto;
    public $resposta;
    public $id_Usuario;
    public $dificuldade;
    public $publica;
    
    /**
    * Configurações e filtros globais do modelo
    * @return Criteria $criteria
    */
    public static function configure(){
        $criteria = new Criteria();
        $criteria->paginate(20, 'paginaQuestao');
        return $criteria;
    }
    
    /**
    * Sanitize - filtra os caracteres válidos para cada atributo
    * Configure corretamente por questão de segurança (XSS)
    * Este método é chamado automaticamente pelo método save() da superclasse
    */
    public function sanitize(){
            $this->id_Questao = 	filter_var($this->id_Questao, FILTER_SANITIZE_NUMBER_INT);
            $this->enunciado = 	$this->htmlFilter($this->enunciado); # vide /lib/core/Record.php
            $this->foto = 	htmlspecialchars($this->foto, ENT_QUOTES, "UTF-8");
            $this->resposta = 	$this->htmlFilter($this->resposta); # vide /lib/core/Record.php
            $this->id_Usuario = 	filter_var($this->id_Usuario, FILTER_SANITIZE_NUMBER_INT);
            $this->dificuldade = 	filter_var($this->dificuldade, FILTER_SANITIZE_NUMBER_INT);
            $this->publica = 	filter_var($this->publica, FILTER_SANITIZE_NUMBER_INT);
    }
    
    /**
    * Questao possui Discursivas
    * @return Discursiva[] array de Discursivas
    */
    function getDiscursivas( $criteria = NULL ) {
        return $this->hasMany('Discursiva','id_Questao_Discursiva',$criteria);
    }
    
    /**
    * Questao possui Objetivas
    * @return Objetiva[] array de Objetivas
    */
    function getObjetivas( $criteria = NULL ) {
        return $this->hasMany('Objetiva','id_Questao_Objetiva',$criteria);
    }
    
    /**
    * Questao pertence a Usuario
    * @return Usuario $Usuario
    */
    function getUsuario() {
        return $this->belongsTo('Usuario','id_Usuario');
    }
    
    /**
    * Questao possui Somatorias
    * @return Somatoria[] array de Somatorias
    */
    function getSomatorias( $criteria = NULL ) {
        return $this->hasMany('Somatoria','id_Questao_Soma',$criteria);
    }
    
    /**
    * Questao possui Tags
    * @return Tag[] array de Tags
    */
    function getTags( $criteria = NULL ) {
        return $this->hasMany('Tag','id_TagQuestao',$criteria);
    }
}