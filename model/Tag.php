<?php

/**
* classe Tag
*
* @author Instalador LazyPHP <http://lazyphp.com.br>
* @version 26/10/2018 08:59
*/
final class Tag extends Record{ 

    const TABLE = 'tag';
    const PK = 'id_Tag';
    
    public $id_Tag;
    public $tag;
    
    /**
    * Configurações e filtros globais do modelo
    * @return Criteria $criteria
    */
    public static function configure(){
        $criteria = new Criteria();
        $criteria->paginate(20, 'paginaTag');
        return $criteria;
    }
    
    /**
    * Sanitize - filtra os caracteres válidos para cada atributo
    * Configure corretamente por questão de segurança (XSS)
    * Este método é chamado automaticamente pelo método save() da superclasse
    */
    public function sanitize(){
            $this->id_Tag = 	filter_var($this->id_Tag, FILTER_SANITIZE_NUMBER_INT);
            $this->tag = 	$this->tag;
    }
    
    /**
    * Tag possui Tag_perguntas
    * @return Tag_pergunta[] array de Tag_perguntas
    */
    function getTag_perguntas( $criteria = NULL ) {
        return $this->hasMany('Tag_pergunta','id_Tag',$criteria);
    }
    
    /**
    * Tag possui Discursivas via Tag_pergunta (NxN)
    * @return Discursiva[] array de Discursivas
    */
    function getTag_perguntaDiscursivas( $criteria = NULL ) {
        return $this->hasNN('Tag_pergunta','id_Tag','id_Discursiva','Discursiva',$criteria);
    }
    
    /**
    * Tag possui Objetivas via Tag_pergunta (NxN)
    * @return Objetiva[] array de Objetivas
    */
    function getTag_perguntaObjetivas( $criteria = NULL ) {
        return $this->hasNN('Tag_pergunta','id_Tag','id_Objetiva','Objetiva',$criteria);
    }
    
    /**
    * Tag possui Somatorias via Tag_pergunta (NxN)
    * @return Somatoria[] array de Somatorias
    */
    function getTag_perguntaSomatorias( $criteria = NULL ) {
        return $this->hasNN('Tag_pergunta','id_Tag','id_Somatoria','Somatoria',$criteria);
    }
}