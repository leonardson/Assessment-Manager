<?php

/**
* classe Usuario
*
* @author Instalador LazyPHP <http://lazyphp.com.br>
* @version 17/10/2018 19:46
*/
final class Usuario extends Record{ 

    const TABLE = 'usuario';
    const PK = 'id_usuario';
    
    public $id_usuario;
    public $matricula;
    public $nome;
    public $sobrenome;
    public $foto;
    public $email;
    public $senha;
    public $login;
    public $nascimento;
    public $descricao;
    public $cabecalho;
    
    /**
    * Configurações e filtros globais do modelo
    * @return Criteria $criteria
    */
    public static function configure(){
        $criteria = new Criteria();
        $criteria->paginate(20, 'paginaUsuario');
        return $criteria;
    }
    
    /**
    * Sanitize - filtra os caracteres válidos para cada atributo
    * Configure corretamente por questão de segurança (XSS)
    * Este método é chamado automaticamente pelo método save() da superclasse
    */
    public function sanitize(){
            $this->id_usuario = 	filter_var($this->id_usuario, FILTER_SANITIZE_NUMBER_INT);
            $this->matricula = 	htmlspecialchars($this->matricula, ENT_QUOTES, "UTF-8");
            $this->nome = 	htmlspecialchars($this->nome, ENT_QUOTES, "UTF-8");
            $this->sobrenome = 	htmlspecialchars($this->sobrenome, ENT_QUOTES, "UTF-8");
            $this->foto = 	htmlspecialchars($this->foto, ENT_QUOTES, "UTF-8");
            $this->email = 	htmlspecialchars($this->email, ENT_QUOTES, "UTF-8");
            $this->senha = 	$this->senha;
            $this->login = 	htmlspecialchars($this->login, ENT_QUOTES, "UTF-8");
            $this->nascimento = 	htmlspecialchars($this->nascimento, ENT_QUOTES, "UTF-8");
            $this->descricao = 	$this->htmlFilter($this->descricao); # vide /lib/core/Record.php
            $this->cabecalho = 	$this->htmlFilter($this->cabecalho); # vide /lib/core/Record.php
    }
    
    /**
    * Usuario possui Questaos
    * @return Questao[] array de Questaos
    */
    function getQuestaos( $criteria = NULL ) {
        return $this->hasMany('Questao','id_Usuario',$criteria);
    }
}