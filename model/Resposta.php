<?php

/**
* classe Resposta
*
* @author Instalador LazyPHP <http://lazyphp.com.br>
* @version 22/10/2018 11:14
*/
final class Resposta extends Record{ 

    const TABLE = 'resposta';
    const PK = 'id_Resposta';
    
    public $id_Resposta;
    public $texto;
    public $pontuacao;
    public $averigua;
    public $id_Somatoria;
    public $id_Discursiva;
    public $id_Objetiva;
    
    /**
    * Configurações e filtros globais do modelo
    * @return Criteria $criteria
    */
    public static function configure(){
        $criteria = new Criteria();
        $criteria->paginate(20, 'paginaResposta');
        return $criteria;
    }
    
    /**
    * Sanitize - filtra os caracteres válidos para cada atributo
    * Configure corretamente por questão de segurança (XSS)
    * Este método é chamado automaticamente pelo método save() da superclasse
    */
    public function sanitize(){
            $this->id_Resposta = 	filter_var($this->id_Resposta, FILTER_SANITIZE_NUMBER_INT);
            $this->texto = 	$this->htmlFilter($this->texto); # vide /lib/core/Record.php
            $this->pontuacao = 	filter_var($this->pontuacao, FILTER_SANITIZE_NUMBER_INT);
            $this->averigua = 	filter_var($this->averigua, FILTER_SANITIZE_NUMBER_INT);
            $this->id_Somatoria = 	filter_var($this->id_Somatoria, FILTER_SANITIZE_NUMBER_INT);
            $this->id_Discursiva = 	filter_var($this->id_Discursiva, FILTER_SANITIZE_NUMBER_INT);
            $this->id_Objetiva = 	filter_var($this->id_Objetiva, FILTER_SANITIZE_NUMBER_INT);
    }
    
    /**
    * Resposta pertence a Discursiva
    * @return Discursiva $Discursiva
    */
    function getDiscursiva() {
        return $this->belongsTo('Discursiva','id_Discursiva');
    }
    
    /**
    * Resposta pertence a Objetiva
    * @return Objetiva $Objetiva
    */
    function getObjetiva() {
        return $this->belongsTo('Objetiva','id_Objetiva');
    }
    
    /**
    * Resposta pertence a Somatoria
    * @return Somatoria $Somatoria
    */
    function getSomatoria() {
        return $this->belongsTo('Somatoria','id_Somatoria');
    }
}