<?php
$usuario = Session::get('user');
?>
<li>
    <?php
    echo $this->Html->getLink('Início', 'Index', '', array(), array('id' => 'inicio'));
    ?>
</li>
<li>
    <?php
    if ($usuario) {
        echo $this->Html->getLink('Banco de questões', 'Usuario', 'banco', array($usuario->usuario_id), array());
    }
    ?>
</li>
<li>
    <?php
    if($usuario){
        echo $this->Html->getLink('Meu perfil', 'Usuario', 'ver', array($usuario->id_usuario));
    }
    ?>
</li>
<li>
    <?php
    if($usuario){
        echo $this->Html->getLink('Sair', 'Login', 'logout');
    } else {
        echo $this->Html->getLink('Entrar', 'Login', 'login');
    }
    ?>
</li>