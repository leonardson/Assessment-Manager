<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="IFSC.me portal de aprendizagem - LazyPHP.com.br ">
        <meta name="author" content="">
        <link rel="icon" type="image/png" href="<?php echo SITE_PATH ?>/template/default/images/icon.png">
        <?php $this->getHeaders(); ?> 
        <script>
            $(document).ready(function() {
                $("[title='Hosted on free web hosting 000webhost.com. Host your own website for FREE.']").hide();
            });
        </script>

        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->   
        <link rel="icon" sizes="192x192" href="<?php echo SITE_PATH ?>/template/default/images/icon.png">
        <link rel="apple-touch-icon" href="<?php echo SITE_PATH ?>/template/default/images/icon.png">
        <link rel="stylesheet" type="text/css" href="<?php echo SITE_PATH?>/lib/frontend/print.css" media="print" />
        <meta name="msapplication-square310x310logo" content="<?php echo SITE_PATH ?>/template/default/images/icon.png">
        <meta name="theme-color" content="#333">
    </head>

    <body>
        <style>
            body{
                background-image: url('<?php echo SITE_PATH?>/template/default/images/background.jpg');
                background-repeat: no-repeat;
                background-attachment: fixed;
                background-size: cover;
                overflow-x: hidden;
            }
            .uk-navbar-container.uk-navbar-sticky {
                background-color: #2EC7D1
            }
        </style>
        <?php
        $cor = 'uk-dark';
        if(Session::get('inicio')) {
            $cor = 'uk-light';
        }
        ?>
        <div uk-sticky="animation: uk-animation-slide-top; sel-target: .uk-navbar-container; cls-active: uk-navbar-sticky uk-light; cls-inactive: uk-navbar-transparent <?php echo $cor ?>; top: 100">
            <nav class="uk-navbar-container uk-navbar-transparent uk-position-top" uk-navbar>

                <div class="uk-navbar-left">

                    <a class="uk-navbar-item uk-logo uk-light" href="<?php echo $this->Html->getUrl('Index', ''); ?>">
                        GeraGeraDor
                    </a>

                </div>

                <div class="uk-navbar-right">

                    <ul class="uk-navbar-nav">
                        <?php
                        include 'template/menu.php';
                        ?>
                    </ul>

                </div>

            </nav>
        </div>
        <div style="min-height: 100vh">

            <?php
            $this->getContents();
            ?>
            <div id="modal" uk-modal>
                <div class="uk-modal-dialog uk-modal-body">
                    <div style="width: 100%; text-align: center">
                        <div uk-spinner></div>
                    </div>
                </div>
                <button class="uk-modal-close-default" type="button" uk-close></button>
            </div>

        </div><!-- /.container -->

    </body>
</html>
