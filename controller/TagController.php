<?php

/**
* classe TagController
*
* @author Instalador LazyPHP <http://lazyphp.com.br>
* @version 26/10/2018 08:59
*/
final class TagController extends AppController{ 

    # página inicial do módulo Tag
    function inicio(){
        $this->setTitle('Tag');
    }

    # lista de Tags
    # renderiza a visão /view/Tag/lista.php
    function lista(){
        $this->setTitle('Tags');
        $c = new Criteria();
        if ( $this->getParam('pesquisa') ) {
            $c->addCondition('tag', 'LIKE', '%' . $this->getParam('pesquisa') . '%');
        }
        if ($this->getParam('ordenaPor')) {
            $c->setOrder($this->getParam('ordenaPor'));
        }
        $this->set('Tags', Tag::getList($c));
    }

    # visualiza um(a) Tag
    # renderiza a visão /ver/Tag/ver.php
    function ver(){
        try {
            $Tag = new Tag( (int)$this->getParam(0) );
            $this->set('Tag', $Tag);
            $this->set('Tag_perguntas',$Tag->getTag_perguntas());
            $this->set('Tag_perguntaDiscursivas',$Tag->getTag_perguntaDiscursivas());
            $this->set('Tag_perguntaObjetivas',$Tag->getTag_perguntaObjetivas());
            $this->set('Tag_perguntaSomatorias',$Tag->getTag_perguntaSomatorias());
            $this->setTitle($Tag->tag);
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('Tag', 'lista');
        }
    }

    # formulário de cadastro de Tag
    # renderiza a visão /view/Tag/cadastrar.php
    function cadastrar(){
        $this->setTitle('Cadastrar Tag');
        $this->set('Tag', new Tag);
    }

    # recebe os dados enviados via post do cadastro de Tag
    # (true)redireciona ou (false) renderiza a visão /view/Tag/cadastrar.php
    function post_cadastrar(){
        $this->setTitle('Cadastrar Tag');
        $Tag = new Tag();
        $this->set('Tag', $Tag);
        try {
            $Tag->id_Tag = filter_input(INPUT_POST , 'id_Tag');
            $Tag->tag = md5(Config::get('salt') . filter_input(INPUT_POST , 'tag'));
            $Tag->save();
            new Msg('Tag salvo(a)!');
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
        }
        if(filter_input(INPUT_POST , 'url_origem')){
            $this->goUrl(Cript::decript(filter_input(INPUT_POST , 'url_origem')));
        }
        $this->go('Tag', 'lista');
    }

    # formulário de edição de Tag
    # renderiza a visão /view/Tag/editar.php
    function editar(){
        $this->setTitle('Editar Tag');
        try {
            $this->set('Tag', new Tag((int) $this->getParam(0)));
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
            $this->go('Tag', 'lista');
        }
    }

    # recebe os dados enviados via post da edição de Tag
    # (true)redireciona ou (false) renderiza a visão /view/Tag/editar.php
    function post_editar(){
        $this->setTitle('Editar Tag');
        try {
            $Tag = new Tag( (int)$this->getParam(0) );
            $this->set('Tag', $Tag);
            $password = filter_input(INPUT_POST , 'tag');
            if(!empty($password)){
                $Tag->tag = md5(Config::get('salt') . $password);
            }
            $Tag->save();
            new Msg('Atualização concluída!');
        } catch (Exception $e) {
            new Msg('A atualização não foi concluída! '.$e->getMessage(),3);
        }
        if(filter_input(INPUT_POST , 'url_origem')){
            $this->goUrl(Cript::decript(filter_input(INPUT_POST , 'url_origem')));
        }
        $this->go('Tag', 'lista');
    }

    # Confirma a exclusão ou não de um(a) Tag
    # renderiza a /view/Tag/apagar.php
    function apagar(){
        $this->setTitle('Apagar Tag');
        try {
            $this->set('Tag', new Tag((int)$this->getParam(0)));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('Tag', 'lista');
        }
    }

    # Recebe o id via post e exclui um(a) Tag
    # redireciona para Tag/lista
    function post_apagar(){
        try {
            $Tag = new Tag((int) filter_input(INPUT_POST , 'id'));
            $Tag->delete();
            new Msg('Tag excluído(a)!', 1);
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
        }
        if(filter_input(INPUT_POST , 'url_origem')){
            $this->goUrl(Cript::decript(filter_input(INPUT_POST , 'url_origem')));
        }
        $this->go('Tag', 'lista');
    }

}