<?php

/**
* classe Tag_perguntaController
*
* @author Instalador LazyPHP <http://lazyphp.com.br>
* @version 26/10/2018 08:59
*/
final class Tag_perguntaController extends AppController{ 

    # página inicial do módulo Tag_pergunta
    function inicio(){
        $this->setTitle('Tag_pergunta');
    }

    # lista de Tag_perguntas
    # renderiza a visão /view/Tag_pergunta/lista.php
    function lista(){
        $this->setTitle('Tag_perguntas');
        $c = new Criteria();
        if ( $this->getParam('pesquisa') ) {
            $c->addCondition('id_Tag', 'LIKE', '%' . $this->getParam('pesquisa') . '%');
        }
        if ($this->getParam('ordenaPor')) {
            $c->setOrder($this->getParam('ordenaPor'));
        }
        $this->set('Tag_perguntas', Tag_pergunta::getList($c));
    }

    # visualiza um(a) Tag_pergunta
    # renderiza a visão /ver/Tag_pergunta/ver.php
    function ver(){
        try {
            $Tag_pergunta = new Tag_pergunta( (int)$this->getParam(0) );
            $this->set('Tag_pergunta', $Tag_pergunta);
            $this->setTitle($Tag_pergunta->id_Tag);
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('Tag_pergunta', 'lista');
        }
    }

    # formulário de cadastro de Tag_pergunta
    # renderiza a visão /view/Tag_pergunta/cadastrar.php
    function cadastrar(){
        $this->setTitle('Cadastrar Tag_pergunta');
        $this->set('Tag_pergunta', new Tag_pergunta);
        $this->set('Discursivas',  Discursiva::getList());
        $this->set('Objetivas',  Objetiva::getList());
        $this->set('Somatorias',  Somatoria::getList());
        $this->set('Tags',  Tag::getList());
    }

    # recebe os dados enviados via post do cadastro de Tag_pergunta
    # (true)redireciona ou (false) renderiza a visão /view/Tag_pergunta/cadastrar.php
    function post_cadastrar(){
        $this->setTitle('Cadastrar Tag_pergunta');
        $Tag_pergunta = new Tag_pergunta();
        $this->set('Tag_pergunta', $Tag_pergunta);
        try {
            $Tag_pergunta->id_Tag_Pergunta = filter_input(INPUT_POST , 'id_Tag_Pergunta');
            $Tag_pergunta->id_Tag = filter_input(INPUT_POST , 'id_Tag');
            $Tag_pergunta->id_Discursiva = filter_input(INPUT_POST , 'id_Discursiva');
            $Tag_pergunta->id_Objetiva = filter_input(INPUT_POST , 'id_Objetiva');
            $Tag_pergunta->id_Somatoria = filter_input(INPUT_POST , 'id_Somatoria');
            $Tag_pergunta->save();
            new Msg('Tag_pergunta salvo(a)!');
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
        }
        if(filter_input(INPUT_POST , 'url_origem')){
            $this->goUrl(Cript::decript(filter_input(INPUT_POST , 'url_origem')));
        }
        $this->go('Tag_pergunta', 'lista');
        $this->set('Discursivas',  Discursiva::getList());
        $this->set('Objetivas',  Objetiva::getList());
        $this->set('Somatorias',  Somatoria::getList());
        $this->set('Tags',  Tag::getList());
    }

    # formulário de edição de Tag_pergunta
    # renderiza a visão /view/Tag_pergunta/editar.php
    function editar(){
        $this->setTitle('Editar Tag_pergunta');
        try {
            $this->set('Tag_pergunta', new Tag_pergunta((int) $this->getParam(0)));
            $this->set('Discursivas',  Discursiva::getList());
            $this->set('Objetivas',  Objetiva::getList());
            $this->set('Somatorias',  Somatoria::getList());
            $this->set('Tags',  Tag::getList());
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
            $this->go('Tag_pergunta', 'lista');
        }
    }

    # recebe os dados enviados via post da edição de Tag_pergunta
    # (true)redireciona ou (false) renderiza a visão /view/Tag_pergunta/editar.php
    function post_editar(){
        $this->setTitle('Editar Tag_pergunta');
        try {
            $Tag_pergunta = new Tag_pergunta( (int)$this->getParam(0) );
            $this->set('Tag_pergunta', $Tag_pergunta);
            $Tag_pergunta->id_Tag = filter_input(INPUT_POST , 'id_Tag');
            $Tag_pergunta->id_Discursiva = filter_input(INPUT_POST , 'id_Discursiva');
            $Tag_pergunta->id_Objetiva = filter_input(INPUT_POST , 'id_Objetiva');
            $Tag_pergunta->id_Somatoria = filter_input(INPUT_POST , 'id_Somatoria');
            $Tag_pergunta->save();
            new Msg('Atualização concluída!');
        } catch (Exception $e) {
            new Msg('A atualização não foi concluída! '.$e->getMessage(),3);
        }
        if(filter_input(INPUT_POST , 'url_origem')){
            $this->goUrl(Cript::decript(filter_input(INPUT_POST , 'url_origem')));
        }
        $this->go('Tag_pergunta', 'lista');
        $this->set('Discursivas',  Discursiva::getList());
        $this->set('Objetivas',  Objetiva::getList());
        $this->set('Somatorias',  Somatoria::getList());
        $this->set('Tags',  Tag::getList());
    }

    # Confirma a exclusão ou não de um(a) Tag_pergunta
    # renderiza a /view/Tag_pergunta/apagar.php
    function apagar(){
        $this->setTitle('Apagar Tag_pergunta');
        try {
            $this->set('Tag_pergunta', new Tag_pergunta((int)$this->getParam(0)));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('Tag_pergunta', 'lista');
        }
    }

    # Recebe o id via post e exclui um(a) Tag_pergunta
    # redireciona para Tag_pergunta/lista
    function post_apagar(){
        try {
            $Tag_pergunta = new Tag_pergunta((int) filter_input(INPUT_POST , 'id'));
            $Tag_pergunta->delete();
            new Msg('Tag_pergunta excluído(a)!', 1);
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
        }
        if(filter_input(INPUT_POST , 'url_origem')){
            $this->goUrl(Cript::decript(filter_input(INPUT_POST , 'url_origem')));
        }
        $this->go('Tag_pergunta', 'lista');
    }

}