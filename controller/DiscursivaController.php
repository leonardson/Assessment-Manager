<?php

/**
 * classe DiscursivaController
 *
 * @author Instalador LazyPHP <http://lazyphp.com.br>
 * @version 17/10/2018 22:52
 */
final class DiscursivaController extends AppController {
    # página inicial do módulo Discursiva

    function inicio() {
        $this->setTitle('Discursiva');
    }

    # lista de Discursivas
    # renderiza a visão /view/Discursiva/lista.php

    function lista() {
        $this->setTitle('Discursivas');
        $c = new Criteria();
        if ($this->getParam('pesquisa')) {
            $c->addCondition('texto', 'LIKE', '%' . $this->getParam('pesquisa') . '%');
        }
        if ($this->getParam('ordenaPor')) {
            $c->setOrder($this->getParam('ordenaPor'));
        }
        $this->set('Discursivas', Discursiva::getList($c));
    }

    # visualiza um(a) Discursiva
    # renderiza a visão /ver/Discursiva/ver.php

    function ver() {
        try {
            $Discursiva = new Discursiva((int) $this->getParam(0));
            $this->set('Discursiva', $Discursiva);
            $this->set('Repostas', $Discursiva->getRepostas());
            $this->set('RepostaObjetivas', $Discursiva->getRepostaObjetivas());
            $this->set('RepostaSomatorias', $Discursiva->getRepostaSomatorias());
            $this->setTitle($Discursiva->texto);
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('Discursiva', 'lista');
        }
    }

    # formulário de cadastro de Discursiva
    # renderiza a visão /view/Discursiva/cadastrar.php

    function cadastrar() {
        $this->setTitle('Cadastrar Discursiva');
        $this->set('Discursiva', new Discursiva);
    }

    # recebe os dados enviados via post do cadastro de Discursiva
    # (true)redireciona ou (false) renderiza a visão /view/Discursiva/cadastrar.php

    function post_cadastrar() {
        $this->setTitle('Cadastrar discursiva');
        $Discursiva = new Discursiva();
        $this->set('Discursiva', $Discursiva);
        try {
            $Discursiva->id_Discursiva = filter_input(INPUT_POST, 'id_Discursiva');
            $Discursiva->texto = filter_input(INPUT_POST, 'texto');
            $Discursiva->dificuldade = (int) filter_input(INPUT_POST, 'dificuldade');
            $usuario = Session::get('user');
            $Discursiva->id_usuario = $usuario->id_usuario;

            $Discursiva->save();
            
            $Resposta = new Resposta();
            $Resposta->texto = (int) filter_input(INPUT_POST, 'linhas');
            $Resposta->id_Discursiva = $Discursiva->id_Discursiva;

            $Resposta->save();

            $tags = explode(';', filter_input(INPUT_POST, 'tags'));
            foreach ($tags as $t) {
                $t = trim($t);
                $t = str_split($t);
                
                if($t[0] != '#') {                    
                    array_unshift($t, '#');
                }
                $t = implode('', $t);
                $nova_tag = new Tag();
                $nova_tag->tag = $t;
                $nova_tag->save();
                $tag_pergunta = new Tag_pergunta();
                $tag_pergunta->id_Tag = $nova_tag->id_Tag;
                $tag_pergunta->id_Discursiva = $Discursiva->id_Discursiva;
                $tag_pergunta->save();
            }
            new Msg('Discursiva salvo(a)!');
        } catch (Exception $e) {
            new Msg($e->getMessage(), 3);
        }
        if (filter_input(INPUT_POST, 'url_origem')) {
            $this->goUrl(Cript::decript(filter_input(INPUT_POST, 'url_origem')));
        }
        $this->go('Usuario', 'banco');
    }

    # formulário de edição de Discursiva
    # renderiza a visão /view/Discursiva/editar.php

    function editar() {
        $this->setTitle('Editar Discursiva');
        try {
            $this->set('Discursiva', new Discursiva((int) $this->getParam(0)));
            $this->set('Usuarios', Usuario::getList());
        } catch (Exception $e) {
            new Msg($e->getMessage(), 3);
            $this->go('Discursiva', 'lista');
        }
    }

    # recebe os dados enviados via post da edição de Discursiva
    # (true)redireciona ou (false) renderiza a visão /view/Discursiva/editar.php

    function post_editar() {
        $this->setTitle('Editar Discursiva');
        try {
            $Discursiva = new Discursiva((int) $this->getParam(0));
            $Discursiva->texto = filter_input(INPUT_POST, 'texto');
            $Discursiva->dificuldade = (int) filter_input(INPUT_POST, 'dificuldade');
            $usuario = Session::get('user');
            $Discursiva->id_usuario = $usuario->id_usuario;

            $Discursiva->save();
            
            $Resposta = new Resposta();
            $Resposta->texto = (int) filter_input(INPUT_POST, 'linhas');
            $Resposta->id_Discursiva = $Discursiva->id_Discursiva;

            $Resposta->save();

            $tags = explode(';', filter_input(INPUT_POST, 'tags'));
            foreach ($tags as $t) {
                $t = trim($t);
                $t = str_split($t);
                if(!$t) {
                    continue;
                }
                if($t[0] != '#') {                    
                    array_unshift($t, '#');
                }
                $t = implode('', $t);
                $nova_tag = new Tag();
                $nova_tag->tag = $t;
                $nova_tag->save();
                $tag_pergunta = new Tag_pergunta();
                $tag_pergunta->id_Tag = $nova_tag->id_Tag;
                $tag_pergunta->id_Discursiva = $Discursiva->id_Discursiva;
                $tag_pergunta->save();
            }
            new Msg('Discursiva salvo(a)!');
        } catch (Exception $e) {
            new Msg('A atualização não foi concluída! ' . $e->getMessage(), 3);
        }
        if (filter_input(INPUT_POST, 'url_origem')) {
            $this->goUrl(Cript::decript(filter_input(INPUT_POST, 'url_origem')));
        }
        $this->go('Discursiva', 'lista');
        $this->set('Usuarios', Usuario::getList());
    }

    # Confirma a exclusão ou não de um(a) Discursiva
    # renderiza a /view/Discursiva/apagar.php

    function apagar() {
        $this->setTitle('Apagar Discursiva');
        try {
            $this->set('Discursiva', new Discursiva((int) $this->getParam(0)));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('Discursiva', 'lista');
        }
    }

    # Recebe o id via post e exclui um(a) Discursiva
    # redireciona para Discursiva/lista

    function post_apagar() {
        try {
            $Discursiva = new Discursiva((int) filter_input(INPUT_POST, 'id'));
            $Discursiva->delete();
            new Msg('Discursiva excluído(a)!', 1);
        } catch (Exception $e) {
            new Msg($e->getMessage(), 3);
        }
        $user = Session::get('user');
        $this->go('Usuario', 'banco', array($user->id));
    }

}
