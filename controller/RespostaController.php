<?php

/**
* classe RespostaController
*
* @author Instalador LazyPHP <http://lazyphp.com.br>
* @version 22/10/2018 11:14
*/
final class RespostaController extends AppController{ 

    # página inicial do módulo Resposta
    function inicio(){
        $this->setTitle('Resposta');
    }

    # lista de Respostas
    # renderiza a visão /view/Resposta/lista.php
    function lista(){
        $this->setTitle('Respostas');
        $c = new Criteria();
        if ( $this->getParam('pesquisa') ) {
            $c->addCondition('texto', 'LIKE', '%' . $this->getParam('pesquisa') . '%');
        }
        if ($this->getParam('ordenaPor')) {
            $c->setOrder($this->getParam('ordenaPor'));
        }
        $this->set('Respostas', Resposta::getList($c));
    }

    # visualiza um(a) Resposta
    # renderiza a visão /ver/Resposta/ver.php
    function ver(){
        try {
            $Resposta = new Resposta( (int)$this->getParam(0) );
            $this->set('Resposta', $Resposta);
            $this->setTitle($Resposta->texto);
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('Resposta', 'lista');
        }
    }

    # formulário de cadastro de Resposta
    # renderiza a visão /view/Resposta/cadastrar.php
    function cadastrar(){
        $this->setTitle('Cadastrar Resposta');
        $this->set('Resposta', new Resposta);
        $this->set('Discursivas',  Discursiva::getList());
        $this->set('Objetivas',  Objetiva::getList());
        $this->set('Somatorias',  Somatoria::getList());
    }

    # recebe os dados enviados via post do cadastro de Resposta
    # (true)redireciona ou (false) renderiza a visão /view/Resposta/cadastrar.php
    function post_cadastrar(){
        $this->setTitle('Cadastrar Resposta');
        $Resposta = new Resposta();
        $this->set('Resposta', $Resposta);
        try {
            $Resposta->id_Resposta = filter_input(INPUT_POST , 'id_Resposta');
            $Resposta->texto = filter_input(INPUT_POST , 'texto');
            $Resposta->pontuacao = filter_input(INPUT_POST , 'pontuacao');
            $Resposta->averigua = filter_input(INPUT_POST , 'averigua');
            $Resposta->id_Somatoria = filter_input(INPUT_POST , 'id_Somatoria');
            $Resposta->id_Discursiva = filter_input(INPUT_POST , 'id_Discursiva');
            $Resposta->id_Objetiva = filter_input(INPUT_POST , 'id_Objetiva');
            $Resposta->save();
            new Msg('Resposta salvo(a)!');
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
        }
        if(filter_input(INPUT_POST , 'url_origem')){
            $this->goUrl(Cript::decript(filter_input(INPUT_POST , 'url_origem')));
        }
        $this->go('Resposta', 'lista');
        $this->set('Discursivas',  Discursiva::getList());
        $this->set('Objetivas',  Objetiva::getList());
        $this->set('Somatorias',  Somatoria::getList());
    }

    # formulário de edição de Resposta
    # renderiza a visão /view/Resposta/editar.php
    function editar(){
        $this->setTitle('Editar Resposta');
        try {
            $this->set('Resposta', new Resposta((int) $this->getParam(0)));
            $this->set('Discursivas',  Discursiva::getList());
            $this->set('Objetivas',  Objetiva::getList());
            $this->set('Somatorias',  Somatoria::getList());
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
            $this->go('Resposta', 'lista');
        }
    }

    # recebe os dados enviados via post da edição de Resposta
    # (true)redireciona ou (false) renderiza a visão /view/Resposta/editar.php
    function post_editar(){
        $this->setTitle('Editar Resposta');
        try {
            $Resposta = new Resposta( (int)$this->getParam(0) );
            $this->set('Resposta', $Resposta);
            $Resposta->texto = filter_input(INPUT_POST , 'texto');
            $Resposta->pontuacao = filter_input(INPUT_POST , 'pontuacao');
            $Resposta->averigua = filter_input(INPUT_POST , 'averigua');
            $Resposta->id_Somatoria = filter_input(INPUT_POST , 'id_Somatoria');
            $Resposta->id_Discursiva = filter_input(INPUT_POST , 'id_Discursiva');
            $Resposta->id_Objetiva = filter_input(INPUT_POST , 'id_Objetiva');
            $Resposta->save();
            new Msg('Atualização concluída!');
        } catch (Exception $e) {
            new Msg('A atualização não foi concluída! '.$e->getMessage(),3);
        }
        if(filter_input(INPUT_POST , 'url_origem')){
            $this->goUrl(Cript::decript(filter_input(INPUT_POST , 'url_origem')));
        }
        $this->go('Resposta', 'lista');
        $this->set('Discursivas',  Discursiva::getList());
        $this->set('Objetivas',  Objetiva::getList());
        $this->set('Somatorias',  Somatoria::getList());
    }

    # Confirma a exclusão ou não de um(a) Resposta
    # renderiza a /view/Resposta/apagar.php
    function apagar(){
        $this->setTitle('Apagar Resposta');
        try {
            $this->set('Resposta', new Resposta((int)$this->getParam(0)));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('Resposta', 'lista');
        }
    }

    # Recebe o id via post e exclui um(a) Resposta
    # redireciona para Resposta/lista
    function post_apagar(){
        try {
            $Resposta = new Resposta((int) filter_input(INPUT_POST , 'id'));
            $Resposta->delete();
            new Msg('Resposta excluído(a)!', 1);
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
        }
        if(filter_input(INPUT_POST , 'url_origem')){
            $this->goUrl(Cript::decript(filter_input(INPUT_POST , 'url_origem')));
        }
        $this->go('Resposta', 'lista');
    }

}