<?php

/**
* classe SomatoriaController
*
* @author Instalador LazyPHP <http://lazyphp.com.br>
* @version 17/10/2018 22:52
*/
final class SomatoriaController extends AppController{ 

    # página inicial do módulo Somatoria
    function inicio(){
        $this->setTitle('Somatoria');
    }

    # lista de Somatorias
    # renderiza a visão /view/Somatoria/lista.php
    function lista(){
        $this->setTitle('Somatorias');
        $c = new Criteria();
        if ( $this->getParam('pesquisa') ) {
            $c->addCondition('texto', 'LIKE', '%' . $this->getParam('pesquisa') . '%');
        }
        if ($this->getParam('ordenaPor')) {
            $c->setOrder($this->getParam('ordenaPor'));
        }
        $this->set('Somatorias', Somatoria::getList($c));
    }

    # visualiza um(a) Somatoria
    # renderiza a visão /ver/Somatoria/ver.php
    function ver(){
        try {
            $Somatoria = new Somatoria( (int)$this->getParam(0) );
            $this->set('Somatoria', $Somatoria);
            $this->set('Repostas',$Somatoria->getRepostas());
            $this->set('RepostaDiscursivas',$Somatoria->getRepostaDiscursivas());
            $this->set('RepostaObjetivas',$Somatoria->getRepostaObjetivas());
            $this->setTitle($Somatoria->texto);
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('Somatoria', 'lista');
        }
    }

    # formulário de cadastro de Somatoria
    # renderiza a visão /view/Somatoria/cadastrar.php
    function cadastrar(){
        $this->setTitle('Cadastrar Somatoria');
        $this->set('Somatoria', new Somatoria);
        $this->set('Usuarios',  Usuario::getList());
    }

    # recebe os dados enviados via post do cadastro de Somatoria
    # (true)redireciona ou (false) renderiza a visão /view/Somatoria/cadastrar.php
    function post_cadastrar(){
        $this->setTitle('Cadastrar Somatoria');
        $Somatoria = new Somatoria();
        $this->set('Somatoria', $Somatoria);
        try {
            $Somatoria->id_Somatoria = filter_input(INPUT_POST , 'id_Somatoria');
            $Somatoria->texto = filter_input(INPUT_POST , 'texto');
            $Somatoria->dificuldade = filter_input(INPUT_POST , 'dificuldade' );
            $usuario = Session::get('user');
            $Somatoria->id_usuario = $usuario->id_usuario;
            
            $Somatoria->save();
            
            $alternativas = json_decode(filter_input(INPUT_POST, 'alternativas'));
            foreach($alternativas as $a) {
                $resposta = new Resposta();
                $resposta->id_Somatoria = $Somatoria->id_Somatoria;
                $resposta->texto = $a->valor;
                $resposta->averigua = $a->isCorrect;
                $resposta->save();
            }
            
            $tags = explode(';', filter_input(INPUT_POST, 'tags'));
            foreach ($tags as $t) {
                $t = trim($t);
                $t = str_split($t);
                
                if($t[0] != '#') {                    
                    array_unshift($t, '#');
                }
                $t = implode('', $t);
                $nova_tag = new Tag();
                $nova_tag->tag = $t;
                $nova_tag->save();
                $tag_pergunta = new Tag_pergunta();
                $tag_pergunta->id_Tag = $nova_tag->id_Tag;
                $tag_pergunta->id_Somatoria = $Somatoria->id_Somatoria;
                $tag_pergunta->save();
            }
            new Msg('Somatoria salvo(a)!');
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
        }
        if(filter_input(INPUT_POST , 'url_origem')){
            $this->goUrl(Cript::decript(filter_input(INPUT_POST , 'url_origem')));
        }
        $this->go('Usuario', 'banco');
        $this->set('Usuarios',  Usuario::getList());
    }

    # formulário de edição de Somatoria
    # renderiza a visão /view/Somatoria/editar.php
    function editar(){
        $this->setTitle('Editar Somatoria');
        try {
            $this->set('Somatoria', new Somatoria((int) $this->getParam(0)));
            $this->set('Usuarios',  Usuario::getList());
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
            $this->go('Somatoria', 'lista');
        }
    }

    # recebe os dados enviados via post da edição de Somatoria
    # (true)redireciona ou (false) renderiza a visão /view/Somatoria/editar.php
    function post_editar(){
        $this->setTitle('Editar Somatoria');
        try {
            $Somatoria = new Somatoria( (int)$this->getParam(0) );
            $this->set('Somatoria', $Somatoria);
            $Somatoria->texto = filter_input(INPUT_POST , 'texto');
            $Somatoria->checking = (bool)filter_input(INPUT_POST , 'checking' );
            $Somatoria->dificuldade = (bool)filter_input(INPUT_POST , 'dificuldade' );
            $Somatoria->id_usuario = filter_input(INPUT_POST , 'id_usuario');
            $Somatoria->save();
            new Msg('Atualização concluída!');
        } catch (Exception $e) {
            new Msg('A atualização não foi concluída! '.$e->getMessage(),3);
        }
        if(filter_input(INPUT_POST , 'url_origem')){
            $this->goUrl(Cript::decript(filter_input(INPUT_POST , 'url_origem')));
        }
        $this->go('Somatoria', 'lista');
        $this->set('Usuarios',  Usuario::getList());
    }

    # Confirma a exclusão ou não de um(a) Somatoria
    # renderiza a /view/Somatoria/apagar.php
    function apagar(){
        $this->setTitle('Apagar Somatoria');
        try {
            $this->set('Somatoria', new Somatoria((int)$this->getParam(0)));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('Somatoria', 'lista');
        }
    }

    # Recebe o id via post e exclui um(a) Somatoria
    # redireciona para Somatoria/lista
    function post_apagar(){
        try {
            $Somatoria = new Somatoria((int) filter_input(INPUT_POST , 'id'));
            $Somatoria->delete();
            new Msg('Somatoria excluído(a)!', 1);
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
        }
        $user = Session::get('user');
        $this->go('Usuario', 'banco', array($user->id));
    }

}