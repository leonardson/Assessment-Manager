<?php

class IndexController extends AppController {

    public function index() {
        // Deixar navbar clara 'uk-light'
        Session::set('inicio', true);
        $this->setTitle('Início');
    }

}
