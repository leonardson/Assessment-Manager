<?php

/**
 * classe ObjetivaController
 *
 * @author Instalador LazyPHP <http://lazyphp.com.br>
 * @version 17/10/2018 22:52
 */
final class ObjetivaController extends AppController {
    # página inicial do módulo Objetiva

    function inicio() {
        $this->setTitle('Objetiva');
    }

    # lista de Objetivas
    # renderiza a visão /view/Objetiva/lista.php

    function lista() {
        $this->setTitle('Objetivas');
        $c = new Criteria();
        if ($this->getParam('pesquisa')) {
            $c->addCondition('texto', 'LIKE', '%' . $this->getParam('pesquisa') . '%');
        }
        if ($this->getParam('ordenaPor')) {
            $c->setOrder($this->getParam('ordenaPor'));
        }
        $this->set('Objetivas', Objetiva::getList($c));
    }

    # visualiza um(a) Objetiva
    # renderiza a visão /ver/Objetiva/ver.php

    function ver() {
        try {
            $Objetiva = new Objetiva((int) $this->getParam(0));
            $this->set('Objetiva', $Objetiva);
            $this->set('Repostas', $Objetiva->getRepostas());
            $this->set('RepostaDiscursivas', $Objetiva->getRepostaDiscursivas());
            $this->set('RepostaSomatorias', $Objetiva->getRepostaSomatorias());
            $this->setTitle($Objetiva->texto);
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('Objetiva', 'lista');
        }
    }

    # formulário de cadastro de Objetiva
    # renderiza a visão /view/Objetiva/cadastrar.php

    function cadastrar() {
        $this->setTitle('Cadastrar Objetiva');
        $this->set('Objetiva', new Objetiva);
        $this->set('Usuarios', Usuario::getList());
    }

    # recebe os dados enviados via post do cadastro de Objetiva
    # (true)redireciona ou (false) renderiza a visão /view/Objetiva/cadastrar.php

    function post_cadastrar() {
        $this->setTitle('Cadastrar Objetiva');
        $Objetiva = new Objetiva();
        $this->set('Objetiva', $Objetiva);
        try {
            $Objetiva->id_Objetiva = filter_input(INPUT_POST, 'id_Objetiva');
            $Objetiva->texto = filter_input(INPUT_POST, 'texto');
            $Objetiva->dificuldade = filter_input(INPUT_POST, 'dificuldade');
            $usuario = Session::get('user');
            $Objetiva->id_usuario = $usuario->id_usuario;

            $Objetiva->save();
            
            $alternativas = json_decode(filter_input(INPUT_POST, 'alternativas'));
            foreach($alternativas as $a) {
                $resposta = new Resposta();
                $resposta->id_Objetiva = $Objetiva->id_Objetiva;
                $resposta->texto = $a->valor;
                $resposta->averigua = $a->isCorrect;
                $resposta->save();
            }
            
            $tags = explode(';', filter_input(INPUT_POST, 'tags'));
            foreach ($tags as $t) {
                $t = trim($t);
                $t = str_split($t);
                
                if($t[0] != '#') {                    
                    array_unshift($t, '#');
                }
                $t = implode('', $t);
                $nova_tag = new Tag();
                $nova_tag->tag = $t;
                $nova_tag->save();
                $tag_pergunta = new Tag_pergunta();
                $tag_pergunta->id_Tag = $nova_tag->id_Tag;
                $tag_pergunta->id_Objetiva = $Objetiva->id_Objetiva;
                $tag_pergunta->save();
            }
            new Msg('Objetiva salvo(a)!');
        } catch (Exception $e) {
            new Msg($e->getMessage(), 3);
        }
        if (filter_input(INPUT_POST, 'url_origem')) {
            $this->goUrl(Cript::decript(filter_input(INPUT_POST, 'url_origem')));
        }
        $this->go('Usuario', 'banco');
    }

    # formulário de edição de Objetiva
    # renderiza a visão /view/Objetiva/editar.php

    function editar() {
        $this->setTitle('Editar Objetiva');
        try {
            $this->set('Objetiva', new Objetiva((int) $this->getParam(0)));
            $this->set('Usuarios', Usuario::getList());
        } catch (Exception $e) {
            new Msg($e->getMessage(), 3);
            $this->go('Objetiva', 'lista');
        }
    }

    # recebe os dados enviados via post da edição de Objetiva
    # (true)redireciona ou (false) renderiza a visão /view/Objetiva/editar.php

    function post_editar() {
        $this->setTitle('Editar Objetiva');
        try {
            $Objetiva = new Objetiva((int) $this->getParam(0));
            $this->set('Objetiva', $Objetiva);
            $Objetiva->texto = filter_input(INPUT_POST, 'texto');
            $Objetiva->checking = (bool) filter_input(INPUT_POST, 'checking');
            $Objetiva->dificuldade = (bool) filter_input(INPUT_POST, 'dificuldade');
            $Objetiva->id_usuario = filter_input(INPUT_POST, 'id_usuario');
            $Objetiva->save();
            new Msg('Atualização concluída!');
        } catch (Exception $e) {
            new Msg('A atualização não foi concluída! ' . $e->getMessage(), 3);
        }
        if (filter_input(INPUT_POST, 'url_origem')) {
            $this->goUrl(Cript::decript(filter_input(INPUT_POST, 'url_origem')));
        }
        $this->go('Objetiva', 'lista');
        $this->set('Usuarios', Usuario::getList());
    }

    # Confirma a exclusão ou não de um(a) Objetiva
    # renderiza a /view/Objetiva/apagar.php

    function apagar() {
        $this->setTitle('Apagar Objetiva');
        try {
            $this->set('Objetiva', new Objetiva((int) $this->getParam(0)));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('Objetiva', 'lista');
        }
    }

    # Recebe o id via post e exclui um(a) Objetiva
    # redireciona para Objetiva/lista

    function post_apagar() {
        try {
            $Objetiva = new Objetiva((int) filter_input(INPUT_POST, 'id'));
            $Objetiva->delete();
            new Msg('Objetiva excluído(a)!', 1);
        } catch (Exception $e) {
            new Msg($e->getMessage(), 3);
        }
        $user = Session::get('user');
        $this->go('Usuario', 'banco', array($user->id));
    }

}
