<?php

/**
 * classe UsuarioController
 *
 * @author Instalador LazyPHP <http://lazyphp.com.br>
 * @version 17/10/2018 19:46
 */
final class UsuarioController extends AppController {
    # página inicial do módulo Usuario

    function inicio() {
        $this->setTitle('Usuario');
    }

    # lista de Usuarios
    # renderiza a visão /view/Usuario/lista.php

    function lista() {
        $this->setTitle('Usuarios');
        $c = new Criteria();
        if ($this->getParam('pesquisa')) {
            $c->addCondition('matricula', 'LIKE', '%' . $this->getParam('pesquisa') . '%');
        }
        if ($this->getParam('ordenaPor')) {
            $c->setOrder($this->getParam('ordenaPor'));
        }
        $this->set('Usuarios', Usuario::getList($c));
    }

    # visualiza um(a) Usuario
    # renderiza a visão /ver/Usuario/ver.php

    function ver() {
        try {
            $Usuario = new Usuario((int) $this->getParam(0));
            $this->set('Usuario', $Usuario);
            $this->set('Questaos', $Usuario->getQuestaos());
            $this->setTitle($Usuario->nome);
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('Usuario', 'lista');
        }
    }

    # formulário de cadastro de Usuario
    # renderiza a visão /view/Usuario/cadastrar.php

    function cadastrar() {
        $this->setTitle('Cadastrar Usuario');
        $this->set('Usuario', new Usuario);
    }

    # recebe os dados enviados via post do cadastro de Usuario
    # (true)redireciona ou (false) renderiza a visão /view/Usuario/cadastrar.php

    function post_cadastrar() {
        $this->setTitle('Cadastrar Usuario');
        $Usuario = new Usuario();
        $this->set('Usuario', $Usuario);
        try {
            $Usuario->id_usuario = filter_input(INPUT_POST, 'id_usuario');
            $Usuario->matricula = filter_input(INPUT_POST, 'matricula');
            $Usuario->nome = filter_input(INPUT_POST, 'nome');
            $Usuario->sobrenome = filter_input(INPUT_POST, 'sobrenome');

            # upload de imagem
            $imagem = $_FILES['foto'];
            # se foi enviada alguma imagem
            if ($imagem['name']) {
                $iu = new ImageUploader($imagem, 300);
                $Usuario->foto = $iu->save(uniqid(), 'foto_perfil');
            }
            $Usuario->email = filter_input(INPUT_POST, 'email');
            $Usuario->senha = md5(Config::get('salt') . filter_input(INPUT_POST, 'senha'));
            $Usuario->login = filter_input(INPUT_POST, 'login');
            $Usuario->nascimento = filter_input(INPUT_POST, 'nascimento');
            $Usuario->descricao = filter_input(INPUT_POST, 'descricao');
            $Usuario->save();
            new Msg('Usuario salvo(a)!');
        } catch (Exception $e) {
            new Msg($e->getMessage(), 3);
        }
        if (filter_input(INPUT_POST, 'url_origem')) {
            $this->goUrl(Cript::decript(filter_input(INPUT_POST, 'url_origem')));
        }
        $this->go('login', '');
    }

    # formulário de edição de Usuario
    # renderiza a visão /view/Usuario/editar.php

    function editar() {
        $this->setTitle('Editar Usuario');
        try {
            $this->set('Usuario', new Usuario((int) $this->getParam(0)));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 3);
            $this->go('Usuario', 'lista');
        }
    }

    # recebe os dados enviados via post da edição de Usuario
    # (true)redireciona ou (false) renderiza a visão /view/Usuario/editar.php

    function post_editar() {
        $this->setTitle('Editar Usuario');
        try {
            $Usuario = new Usuario((int) $this->getParam(0));
            $this->set('Usuario', $Usuario);
            $Usuario->matricula = filter_input(INPUT_POST, 'matricula');
            $Usuario->nome = filter_input(INPUT_POST, 'nome');
            $Usuario->sobrenome = filter_input(INPUT_POST, 'sobrenome');
            # upload de imagem
            $imagem = $_FILES['foto'];
            # se foi enviada alguma imagem
            if ($imagem['name']) {
                $iu = new ImageUploader($imagem, 300);
                $Usuario->foto = $iu->save(uniqid(), 'foto_perfil');
            }
            $Usuario->email = filter_input(INPUT_POST, 'email');
            $password = filter_input(INPUT_POST, 'senha');
            if (!empty($password)) {
                $Usuario->senha = md5(Config::get('salt') . $password);
            }
            $Usuario->login = filter_input(INPUT_POST, 'login');
            $Usuario->nascimento = filter_input(INPUT_POST, 'nascimento');
            $Usuario->descricao = filter_input(INPUT_POST, 'descricao');
            $Usuario->save();
            new Msg('Atualização concluída!');
        } catch (Exception $e) {
            new Msg('A atualização não foi concluída! ' . $e->getMessage(), 3);
        }
        if (filter_input(INPUT_POST, 'url_origem')) {
            $this->goUrl(Cript::decript(filter_input(INPUT_POST, 'url_origem')));
        }
        $user = Session::get('user');
        $this->go('Usuario', 'ver', array($user->id_usuario));
    }

    # Confirma a exclusão ou não de um(a) Usuario
    # renderiza a /view/Usuario/apagar.php

    function apagar() {
        $this->setTitle('Apagar Usuario');
        try {
            $this->set('Usuario', new Usuario((int) $this->getParam(0)));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('Usuario', 'lista');
        }
    }

    # Recebe o id via post e exclui um(a) Usuario
    # redireciona para Usuario/lista

    function post_apagar() {
        try {
            $Usuario = new Usuario((int) filter_input(INPUT_POST, 'id'));
            $Usuario->delete();
            new Msg('Usuario excluído(a)!', 1);
        } catch (Exception $e) {
            new Msg($e->getMessage(), 3);
        }
        if (filter_input(INPUT_POST, 'url_origem')) {
            $this->goUrl(Cript::decript(filter_input(INPUT_POST, 'url_origem')));
        }
        $this->go('Usuario', 'lista');
    }

    function banco() {
        $this->setTitle('Banco de questões');
        $c = new Criteria();
        if ($this->getParam('pesquisa')) {
            $c->addCondition('tag', 'LIKE', '%' . $this->getParam('pesquisa') . '%');
        }
        if ($this->getParam('ordenaPor')) {
            $c->setOrder($this->getParam('ordenaPor'));
        }
        $usuario = Session::get('user');
        
        $c->addCondition('id_usuario', '=', $usuario->id_usuario);
        $this->set('Somatorias', Somatoria::getList($c));
        $this->set('Discursivas', Discursiva::getList($c));
        $this->set('Objetivas', Objetiva::getList($c));
        
    }
    
    function nova_prova() {
        $this->setTitle('ao');
        $user = Session::get('user');
        $this->set('Usuario', $user);
    }
    
    function post_nova_prova() {
        $user = Session::get('user');
        $this->set('Usuario', $user);
        try {
            $questoes = $_POST['questao'];
            $quantidade_provas = filter_input(INPUT_POST, 'quantidade_provas');
            $questoes_tratadas = [];
            foreach($questoes as $q) {
                if( substr($q, 0, 1) == 's' ) {
                    $nova_somatoria = new Somatoria(substr( $q, 2 ));
                    array_push($questoes_tratadas, $nova_somatoria);
                }
                elseif( substr($q, 0, 1) == 'd' ) {
                    $nova_discursiva = new Discursiva(substr( $q, 2 ));
                    array_push($questoes_tratadas, $nova_discursiva);    
                }
                else {
                    $nova_objetiva = new Objetiva(substr( $q, 2 ));
                    array_push($questoes_tratadas, $nova_objetiva); 
                }
            }
            $this->set('questoes', $questoes_tratadas);
            $this->set('quantidade_provas', $quantidade_provas);
        } catch (Exception $ex) {
            new Msg($ex->getMessage(), 3);
            $this->go('Usuario', 'banco', array($user->id_usuario));
        }
    }
    
    function nova_questao() {
        $this->setTitle('Nova questão');
    }

}
