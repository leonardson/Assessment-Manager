<?php
# Visão view/Resposta/editar.php
/* @var $this RespostaController */
/* @var $Resposta Resposta */
?>
<div class="Resposta editar panel panel-default">
    <div class="panel-heading">
        <h1>Editar Resposta</h1>
    </div>
    <div class="panel-body">
    <form class="form-horizontal" method="post" role="form" action="<?php echo $this->Html->getUrl('Resposta', 'editar', array($Resposta->id_Resposta)) ?>"  enctype="multipart/form-data">
    <div class="text-info">Os campos marcados com <i class="fa fa-asterisk"></i> são de preenchimento obrigatório.</div>
    <br>
        <?php
            echo $this->Html->getFormHidden('id_Resposta', $Resposta->id_Resposta);
        # texto
        if($this->getParam('texto')){
            echo $this->Html->getFormHidden('texto', $this->getParam('texto'));
        } else {
            echo $this->Html->getFormTextareaHtml('Texto', 'texto', $Resposta->texto, '', false);
        }
        # pontuacao
        if($this->getParam('pontuacao')){
            echo $this->Html->getFormHidden('pontuacao', $this->getParam('pontuacao'));
        } else {
            echo $this->Html->getFormInput('Pontuacao', 'pontuacao', $Resposta->pontuacao, 'number', '', false);
        }
        # averigua
        if($this->getParam('averigua')){
            echo $this->Html->getFormHidden('averigua', $this->getParam('averigua'));
        } else {
            echo $this->Html->getFormInput('Averigua', 'averigua', $Resposta->averigua, 'number', '', false);
        }
        # id_Somatoria
        if ($this->getParam('id_Somatoria')) {
            echo $this->Html->getFormHidden('id_Somatoria', $this->getParam('id_Somatoria'));
        } else {
            echo $this->Html->getFormSelect('Somatoria', 'id_Somatoria', array_columns((array) $Somatorias,'id_Somatoria', 'id_Somatoria'));
        }
        # id_Discursiva
        if ($this->getParam('id_Discursiva')) {
            echo $this->Html->getFormHidden('id_Discursiva', $this->getParam('id_Discursiva'));
        } else {
            echo $this->Html->getFormSelect('Discursiva', 'id_Discursiva', array_columns((array) $Discursivas,'id_Discursiva', 'id_Discursiva'));
        }
        # id_Objetiva
        if ($this->getParam('id_Objetiva')) {
            echo $this->Html->getFormHidden('id_Objetiva', $this->getParam('id_Objetiva'));
        } else {
            echo $this->Html->getFormSelect('Objetiva', 'id_Objetiva', array_columns((array) $Objetivas,'id_Objetiva', 'id_Objetiva'));
        }
        if($this->getParam('url_origem')){
            echo $this->Html->getFormHidden('url_origem', $this->getParam('url_origem'));
        }
        ?>
        <div class="clearfix"></div>
        <div class="text-right">
        <?php
        if($this->getParam('url_origem')){
            $url_destino = Cript::decript($this->getParam('url_origem'));
        } else {
            $url_destino = $this->Html->getUrl('Resposta', 'lista');
        } ?>
            <a href="<?php echo $url_destino ?>" class="btn btn-default" data-dismiss="modal">Cancelar</a>
            <input type="submit" class="btn btn-primary" value="salvar">
        </div>
    </form>
    </div> <!-- .panel-body -->
</div> <!-- .panel -->
<!-- LazyPHP.com.br -->