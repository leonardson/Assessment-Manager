<?php
# Visão view/Resposta/ver.php 
/* @var $this RespostaController */
/* @var $Resposta Resposta */
?>
<div class="ver resposta panel panel-default">
<div class="panel-body">
    <div class="panel panel-info">
        <div class="panel-heading">
            <h1><?php echo $Resposta->texto;?></h1>
        </div>
        <div class="panel-body">
            <div class="atributo pontuacao col-sm-6">
                <div class="name col-md-3"><strong>Pontuacao</strong>: </div>
                <div class="value col-md-9"><?php echo $Resposta->pontuacao;?></div>
            </div>
            <div class="atributo averigua col-sm-6">
                <div class="name col-md-3"><strong>Averigua</strong>: </div>
                <div class="value col-md-9"><?php echo $Resposta->averigua;?></div>
            </div>
            <div class="atributo col-sm-6">
                <div class="col-md-3"><strong>Somatoria</strong>: </div>
                <div class="col-md-9">
                    <?php
                    echo $this->Html->getLink($Resposta->getSomatoria()->texto, 'Somatoria', 'ver',
                        array($Resposta->getSomatoria()->id_Somatoria), // variaveis via GET opcionais
                        array('class' => '')); // atributos HTML opcionais
                ?>
                </div>
            </div>
            <div class="atributo col-sm-6">
                <div class="col-md-3"><strong>Discursiva</strong>: </div>
                <div class="col-md-9">
                    <?php
                    echo $this->Html->getLink($Resposta->getDiscursiva()->texto, 'Discursiva', 'ver',
                        array($Resposta->getDiscursiva()->id_Discursiva), // variaveis via GET opcionais
                        array('class' => '')); // atributos HTML opcionais
                ?>
                </div>
            </div>
            <div class="atributo col-sm-6">
                <div class="col-md-3"><strong>Objetiva</strong>: </div>
                <div class="col-md-9">
                    <?php
                    echo $this->Html->getLink($Resposta->getObjetiva()->texto, 'Objetiva', 'ver',
                        array($Resposta->getObjetiva()->id_Objetiva), // variaveis via GET opcionais
                        array('class' => '')); // atributos HTML opcionais
                ?>
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
    <br>
    

    
</div>
</div>
<!-- LazyPHP.com.br -->