<?php
# Visão view/Resposta/cadastrar.php
/* @var $this RespostaController */
/* @var $Resposta Resposta */
?>
<div class="Resposta cadastrar panel panel-default">
    <div class="panel-heading">
        <h1>Cadastrar Resposta</h1>
    </div>
    <div class="panel-body">
    <form class="form-horizontal" method="post" role="form" action="<?php echo $this->Html->getUrl('Resposta', 'cadastrar') ?>"  enctype="multipart/form-data">
    <div class="text-info">Os campos marcados com <i class="fa fa-asterisk"></i> são de preenchimento obrigatório.</div>
    <br>
        <?php
        # texto
        echo $this->Html->getFormTextareaHtml('Texto', 'texto', $Resposta->texto, '', false);
        # pontuacao
        echo $this->Html->getFormInput('Pontuacao', 'pontuacao', $Resposta->pontuacao, 'number', '', false);
        # averigua
        echo $this->Html->getFormInput('Averigua', 'averigua', $Resposta->averigua, 'number', '', false);
        # id_Somatoria
        if ($this->getParam('id_Somatoria')) {
            echo $this->Html->getFormHidden('id_Somatoria', $this->getParam('id_Somatoria'));
        } else {
            echo $this->Html->getFormSelect('Somatoria', 'id_Somatoria', array_columns((array) $Somatorias,'id_Somatoria', 'id_Somatoria'));
        }
        # id_Discursiva
        if ($this->getParam('id_Discursiva')) {
            echo $this->Html->getFormHidden('id_Discursiva', $this->getParam('id_Discursiva'));
        } else {
            echo $this->Html->getFormSelect('Discursiva', 'id_Discursiva', array_columns((array) $Discursivas,'id_Discursiva', 'id_Discursiva'));
        }
        # id_Objetiva
        if ($this->getParam('id_Objetiva')) {
            echo $this->Html->getFormHidden('id_Objetiva', $this->getParam('id_Objetiva'));
        } else {
            echo $this->Html->getFormSelect('Objetiva', 'id_Objetiva', array_columns((array) $Objetivas,'id_Objetiva', 'id_Objetiva'));
        }
        if($this->getParam('url_origem')){
            echo $this->Html->getFormHidden('url_origem', $this->getParam('url_origem'));
        }
        ?>
        <div class="clearfix"></div>
        <div class="text-right">
        <?php
        if($this->getParam('url_origem')){
            $url_destino = Cript::decript($this->getParam('url_origem'));
        } else {
            $url_destino = $this->Html->getUrl('Resposta', 'lista');
        } ?>
            <a href="<?php echo $url_destino ?>" class="btn btn-default" data-dismiss="modal">Cancelar</a>
            <input type="submit" class="btn btn-primary" value="salvar">
        </div>
    </form>
    </div> <!-- .panel-body -->
</div> <!-- .panel -->
<!-- LazyPHP.com.br -->