

<div class="uk-background-cover uk-dark" uk-parallax="bgy: -100" style="min-height: 100vh; background-image: url('<?php echo SITE_PATH; ?>/template/default/images/background-2.jpg');">

<div class="uk-position-center index-painel uk-light">
    <h1 class="uk-heading-primary uk-animation-scale-up uk-text-center">Gerador de provas</h1>
    <hr class="uk-animation-scale-up">
    <h3 class="uk-text-center uk-animation-scale-up">Uma frase de efeito muito daora!</h3>
    <br>
    <br>
    <div>
        <a href="<?php echo $this->Html->getUrl('login', '') ?>" class="uk-button uk-width-medium uk-animation-scale-up uk-button-secondary uk-align-left">Começar a usar!</a>
        <a href="#" class="uk-button uk-button-secondary uk-width-medium uk-align-right uk-animation-scale-up">Saber mais</a>
    </div>
</div>

</div>
<style>
.uk-overlay-primary{  
background-color: rgba(46, 199, 209, .8) !important;
}
</style>
<div class="uk-cover-container" style="min-height: 100vh;">
    <img src="<?php echo SITE_PATH ?>/template/default/images/background.jpg" uk-cover>
    <div class="uk-overlay uk-overlay-primary uk-position-left" style="width: 60vh; margin:20vh; margin-bottom:20vh">
        <h1 class="uk-text-center">Seu novo jeito de organizar!</h1>
        <p>.Abandone as pastas do seu computador e os pendrives como chaveiro...</p>
        <p>Essa é a ferramenta que você estava precisando!</p>
    </div>
</div>
<div class="uk-cover-container" style="min-height: 100vh;">
    <img src="<?php echo SITE_PATH ?>/template/default/images/background.jpg" uk-cover>
    <div class="uk-overlay uk-overlay-primary uk-position-right" style="width: 60vh; margin:20vh;">
        <h1 class="uk-text-center">Seu novo jeito de organizar!</h1>
        <p>Default Lorem ipsum dolor sit amet, consectetur adipiscing elit.Default Lorem ipsum dolor sit amet, consectetur adipiscing elit.Default Lorem ipsum dolor sit amet, consectetur adipiscing elit.Default Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
    </div>
</div>