<?php
# Visão view/Discursiva/ver.php 
/* @var $this DiscursivaController */
/* @var $Discursiva Discursiva */
?>
<div class="ver discursiva uk-container">
    <div class="uk-card uk-card-body uk-card-default uk-padding">
        <h1 class="uk-card-title"><?php echo $Discursiva->texto;?></h1>
        <?php
        echo 'Essa questão está definida para possuir '.$Discursiva->getRespostas()[0]->texto.' linhas.';
        ?>
        <hr>
        <div class="uk-text-right">
            <?php echo $this->Html->getModalLink('Apagar', 'Discursiva', 'apagar', array($Discursiva->id_Discursiva), array('class' => 'uk-button uk-button-danger modal-trigger'))?>
        </div>
        <div class="clearfix"></div>
    </div>
</div>

<!-- LazyPHP.com.br -->