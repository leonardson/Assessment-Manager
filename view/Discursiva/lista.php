<?php
# Visão view/Discursiva/lista.php 
/* @var $this DiscursivaController */
/* @var $Discursivas Discursiva[] */
?>
<div class="Discursiva lista panel panel-default">
    <!-- titulo da pagina -->
    <div class="panel-heading">
        <h1>Discursivas</h1>
    </div>
        

    <div class="panel-body">
        <!-- botao de cadastro -->
        <div class="text-right pull-right">
            <p><?php echo $this->Html->getLink('<i class="fa fa-plus-circle"></i> Cadastrar Discursiva', 'Discursiva', 'cadastrar', NULL, array('class' => 'btn btn-primary')); ?></p>
        </div>

        <!-- formulario de pesquisa -->
        <div class="pull-left">
            <form class="form-inline" role="form" method="get" action="<?php echo $this->Html->getUrl(CONTROLLER,ACTION,array('ordenaPor'=>$this->getParam('ordenaPor')))?>">
                <div class="form-group">
                    <label class="sr-only" for="pesquisa">Pesquisar</label>
                    <input value="<?php echo $this->getParam('pesquisa') ?>" type="search" class="form-control" name="pesquisa" id="pesquisa" placeholder="Texto">
                </div>
                <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
            </form>
        </div>

        <div class="clearfix"></div>
        <br>
        <!-- tabela de resultados -->
        <div class="table-responsive">
            <table class="table table-hover">
                <thead>
                    <tr>
                        <th>
                            <a href='<?php echo $this->Html->getUrl('Discursiva', 'lista', array('ordenaPor' => 'texto', 'pesquisa' => $this->getParam('pesquisa') )); ?>'>
                                Texto
                            </a>
                        </th>
                        <th>
                            <a href='<?php echo $this->Html->getUrl('Discursiva', 'lista', array('ordenaPor' => 'dificuldade', 'pesquisa' => $this->getParam('pesquisa') )); ?>'>
                                Dificuldade
                            </a>
                        </th>
                        <th>
                            <a href='<?php echo $this->Html->getUrl('Discursiva', 'lista', array('ordenaPor' => 'id_usuario', 'pesquisa' => $this->getParam('pesquisa') )); ?>'>
                                Usuario
                            </a>
                        </th>
                        <th>&nbsp;</th>
                        <th>&nbsp;</th>
                    </tr>
                </thead>
                <?php
                foreach ($Discursivas as $d) {
                    echo '<tr>';
                        echo '<td>';
                        echo $this->Html->getLink($d->texto, 'Discursiva', 'ver',
                            array($d->id_Discursiva), // variaveis via GET opcionais
                            array()); // atributos HTML opcionais
                        echo '</td>';
                        echo '<td>';
                        $check = '<i class="fa fa-times"></i>';
                        if($d->dificuldade){
                            $check = '<i class="fa fa-check"></i>';
                        }
                        echo $this->Html->getLink($check, 'Discursiva', 'ver',
                            array($d->id_Discursiva), // variaveis via GET opcionais
                            array()); // atributos HTML opcionais
                        echo '</td>';
                        echo '<td>';
                        echo $this->Html->getLink($d->getUsuario()->matricula, 'Usuario', 'ver',
                            array($d->getUsuario()->id_usuario), // variaveis via GET opcionais
                            array()); // atributos HTML opcionais
                        echo '</td>';
                        echo '<td width="50">';
                        echo $this->Html->getLink('<i class="fa fa-pencil-square-o"></i>', 'Discursiva', 'editar', 
                            array($d->id_Discursiva), 
                            array('class' => 'text-warning', 'title' => 'editar'));
                        echo '</td>';
                        echo '<td width="50">';
                        echo $this->Html->getModalLink('<i class="fa fa-trash-o"></i>', 'Discursiva', 'apagar', 
                            array($d->id_Discursiva), 
                            array('class' => 'text-danger', 'title' => 'apagar'));
                        echo '</td>';
                    echo '</tr>';
                }
                ?>
            </table>

            <!-- menu de paginação -->
            <div style="text-align:center"><?php echo $Discursivas->getNav(); ?></div>
        </div> <!-- .table-responsive -->
    </div> <!-- .panel-body -->
</div> <!-- .panel -->

<script>
    /* faz a pesquisa com ajax */
    $(document).ready(function() {
        $('#pesquisa').keyup(function() {
            var r = true;
            if (r) {
                r = false;
                $("div.table-responsive").load(
                <?php
                if (isset($_GET['ordenaPor']))
                    echo '"' . $this->Html->getUrl('Discursiva', 'lista', array('ordenaPor' => $_GET['ordenaPor'])) . 'pesquisa:" + encodeURIComponent($("#pesquisa").val()) + " .table-responsive"';
                else
                    echo '"' . $this->Html->getUrl('Discursiva', 'lista') . 'pesquisa:" + encodeURIComponent($("#pesquisa").val()) + " .table-responsive"';
                ?>
                 , function() {
                    r = true;
                });
            }
        });
    });
</script>
<!-- LazyPHP.com.br -->