<?php
# Visão view/Discursiva/editar.php
/* @var $this DiscursivaController */
/* @var $Discursiva Discursiva */
?>

<div class="uk-container uk-background-default uk-margin-large-top uk-margin-large-bottom">
        <h1 class="uk-text-center">Registrar uma pergunta <b>discursiva</b></h1>
        <h5>Os campos marcados com <?php echo $this->Html->getRequired() ?> são de preenchimento obrigatório.</h5>
        <hr>
        <form class="uk-form-stacked uk-margin-small" action="<?php echo $this->Html->getUrl('Discursiva', 'cadastrar') ?>" method="post">
            <?php
            # texto
            echo $this->Html->getFormTextareaHtml('Enunciado ', 'texto', $Discursiva->texto, 'Escreva o enunciado da pergunta', true);
            # dificuldade
            
            echo '<div class="uk-margin">';
            echo '<label for="dificuldade">Escolha a dificuldade da questão</label>';
            echo '<select class="uk-select" name="dificuldade" id="dificuldade">';
            echo '<option value="1" ';
            echo $Discursiva->dificuldade == 1 ? 'selected' : '';
            echo '>Fácil</option>';
            echo '<option value="2" ';
            echo $Discursiva->dificuldade == 2 ? 'selected' : '';
            echo '>Intermediário</option>';
            echo '<option value="3" ';
            echo $Discursiva->dificuldade == 3 ? 'selected' : '';
            echo '>Difícil</option>';
            echo '</select>';
            echo '</div>';
            #quantidade de linhas 
            echo $this->Html->getFormInput('Quantidade de linhas para resposta', 'linhas', $Discursiva->getRespostas()[0]->texto, 'number', 'Quantidade de linhas', true);
            
            #tags
            $tags = '';
            if(sizeof($Discursiva->getTag_perguntaTags()) > 0) {
                foreach($Discursiva->getTag_perguntaTags() as $tag) {
                    $tags .= $tag->tag . '; ';
                }
            } else {
                $tags = $Discursiva->getTag_perguntaTags()[0]->tag;
            }
            echo $this->Html->getFormInput('Tags', 'tags', $tags, 'text', 'Ex: #matematica; #geometria; #equacao...', true);
            
            if ($this->getParam('url_origem')) {
                echo $this->Html->getFormHidden('url_origem', $this->getParam('url_origem'));
            }
            ?>
            <div class="uk-text-right">
                <?php
                if ($this->getParam('url_origem')) {
                    $url_destino = Cript::decript($this->getParam('url_origem'));
                } else {
                    $url_destino = $this->Html->getUrl('Usuario', 'lista');
                }
                ?>
                <a href="<?php echo $this->Html->getUrl('Usuario', 'banco', array(Session::get('user')->id_usuario)) ?>" class="uk-button uk-button-danger uk-margin-small-bottom">Cancelar</a>
                <input type="submit" class="uk-button uk-button-primary uk-margin-small-bottom" value="salvar">
            </div>

        </form>
    </div>