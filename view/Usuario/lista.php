<?php
# Visão view/Usuario/lista.php 
/* @var $this UsuarioController */
/* @var $Usuarios Usuario[] */
?>
<div class="Usuario lista panel panel-default">
    <!-- titulo da pagina -->
    <div class="panel-heading">
        <h1>Usuarios</h1>
    </div>
        

    <div class="panel-body">
        <!-- botao de cadastro -->
        <div class="text-right pull-right">
            <p><?php echo $this->Html->getLink('<i class="fa fa-plus-circle"></i> Cadastrar Usuario', 'Usuario', 'cadastrar', NULL, array('class' => 'btn btn-primary')); ?></p>
        </div>

        <!-- formulario de pesquisa -->
        <div class="pull-left">
            <form class="form-inline" role="form" method="get" action="<?php echo $this->Html->getUrl(CONTROLLER,ACTION,array('ordenaPor'=>$this->getParam('ordenaPor')))?>">
                <div class="form-group">
                    <label class="sr-only" for="pesquisa">Pesquisar</label>
                    <input value="<?php echo $this->getParam('pesquisa') ?>" type="search" class="form-control" name="pesquisa" id="pesquisa" placeholder="Matricula">
                </div>
                <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
            </form>
        </div>

        <div class="clearfix"></div>
        <br>
        <!-- tabela de resultados -->
        <div class="table-responsive">
            <table class="table table-hover">
                <thead>
                    <tr>
                        <th>
                            <a href='<?php echo $this->Html->getUrl('Usuario', 'lista', array('ordenaPor' => 'matricula', 'pesquisa' => $this->getParam('pesquisa') )); ?>'>
                                Matricula
                            </a>
                        </th>
                        <th>
                            <a href='<?php echo $this->Html->getUrl('Usuario', 'lista', array('ordenaPor' => 'nome', 'pesquisa' => $this->getParam('pesquisa') )); ?>'>
                                Nome
                            </a>
                        </th>
                        <th>
                            <a href='<?php echo $this->Html->getUrl('Usuario', 'lista', array('ordenaPor' => 'sobrenome', 'pesquisa' => $this->getParam('pesquisa') )); ?>'>
                                Sobrenome
                            </a>
                        </th>
                        <th>
                            <a href='<?php echo $this->Html->getUrl('Usuario', 'lista', array('ordenaPor' => 'foto', 'pesquisa' => $this->getParam('pesquisa') )); ?>'>
                                Foto
                            </a>
                        </th>
                        <th>
                            <a href='<?php echo $this->Html->getUrl('Usuario', 'lista', array('ordenaPor' => 'email', 'pesquisa' => $this->getParam('pesquisa') )); ?>'>
                                Email
                            </a>
                        </th>
                        <th>
                            <a href='<?php echo $this->Html->getUrl('Usuario', 'lista', array('ordenaPor' => 'senha', 'pesquisa' => $this->getParam('pesquisa') )); ?>'>
                                Senha
                            </a>
                        </th>
                        <th>
                            <a href='<?php echo $this->Html->getUrl('Usuario', 'lista', array('ordenaPor' => 'login', 'pesquisa' => $this->getParam('pesquisa') )); ?>'>
                                Login
                            </a>
                        </th>
                        <th>
                            <a href='<?php echo $this->Html->getUrl('Usuario', 'lista', array('ordenaPor' => 'nascimento', 'pesquisa' => $this->getParam('pesquisa') )); ?>'>
                                Nascimento
                            </a>
                        </th>
                        <th>
                            <a href='<?php echo $this->Html->getUrl('Usuario', 'lista', array('ordenaPor' => 'descricao', 'pesquisa' => $this->getParam('pesquisa') )); ?>'>
                                Descricao
                            </a>
                        </th>
                        <th>&nbsp;</th>
                        <th>&nbsp;</th>
                    </tr>
                </thead>
                <?php
                foreach ($Usuarios as $u) {
                    echo '<tr>';
                        echo '<td>';
                        echo $this->Html->getLink($u->matricula, 'Usuario', 'ver',
                            array($u->id_usuario), // variaveis via GET opcionais
                            array()); // atributos HTML opcionais
                        echo '</td>';
                        echo '<td>';
                        echo $this->Html->getLink($u->nome, 'Usuario', 'ver',
                            array($u->id_usuario), // variaveis via GET opcionais
                            array()); // atributos HTML opcionais
                        echo '</td>';
                        echo '<td>';
                        echo $this->Html->getLink($u->sobrenome, 'Usuario', 'ver',
                            array($u->id_usuario), // variaveis via GET opcionais
                            array()); // atributos HTML opcionais
                        echo '</td>';
                        echo '<td>';
                        echo $this->Html->getLink($u->foto, 'Usuario', 'ver',
                            array($u->id_usuario), // variaveis via GET opcionais
                            array()); // atributos HTML opcionais
                        echo '</td>';
                        echo '<td>';
                        echo $this->Html->getLink($u->email, 'Usuario', 'ver',
                            array($u->id_usuario), // variaveis via GET opcionais
                            array()); // atributos HTML opcionais
                        echo '</td>';
                        echo '<td>';
                        echo $this->Html->getLink('**********', 'Usuario', 'ver',
                            array($u->id_usuario), // variaveis via GET opcionais
                            array()); // atributos HTML opcionais
                        echo '</td>';
                        echo '<td>';
                        echo $this->Html->getLink($u->login, 'Usuario', 'ver',
                            array($u->id_usuario), // variaveis via GET opcionais
                            array()); // atributos HTML opcionais
                        echo '</td>';
                        echo '<td>';
                        $horario = new DateTime($u->nascimento);
                        echo $this->Html->getLink(date_format($horario, 'd/m/Y'), 'Usuario', 'ver',
                            array($u->id_usuario), // variaveis via GET opcionais
                            array()); // atributos HTML opcionais
                        echo '</td>';
                        echo '<td>';
                        echo $this->Html->getLink($u->descricao, 'Usuario', 'ver',
                            array($u->id_usuario), // variaveis via GET opcionais
                            array()); // atributos HTML opcionais
                        echo '</td>';
                        echo '<td width="50">';
                        echo $this->Html->getLink('<i class="fa fa-pencil-square-o"></i>', 'Usuario', 'editar', 
                            array($u->id_usuario), 
                            array('class' => 'text-warning', 'title' => 'editar'));
                        echo '</td>';
                        echo '<td width="50">';
                        echo $this->Html->getModalLink('<i class="fa fa-trash-o"></i>', 'Usuario', 'apagar', 
                            array($u->id_usuario), 
                            array('class' => 'text-danger', 'title' => 'apagar'));
                        echo '</td>';
                    echo '</tr>';
                }
                ?>
            </table>

            <!-- menu de paginação -->
            <div style="text-align:center"><?php echo $Usuarios->getNav(); ?></div>
        </div> <!-- .table-responsive -->
    </div> <!-- .panel-body -->
</div> <!-- .panel -->

<script>
    /* faz a pesquisa com ajax */
    $(document).ready(function() {
        $('#pesquisa').keyup(function() {
            var r = true;
            if (r) {
                r = false;
                $("div.table-responsive").load(
                <?php
                if (isset($_GET['ordenaPor']))
                    echo '"' . $this->Html->getUrl('Usuario', 'lista', array('ordenaPor' => $_GET['ordenaPor'])) . 'pesquisa:" + encodeURIComponent($("#pesquisa").val()) + " .table-responsive"';
                else
                    echo '"' . $this->Html->getUrl('Usuario', 'lista') . 'pesquisa:" + encodeURIComponent($("#pesquisa").val()) + " .table-responsive"';
                ?>
                 , function() {
                    r = true;
                });
            }
        });
    });
</script>
<!-- LazyPHP.com.br -->