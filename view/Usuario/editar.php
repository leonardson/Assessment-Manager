<?php
# Visão view/Usuario/cadastrar.php
/* @var $this UsuarioController */
/* @var $Usuario Usuario */
?>
<div class="uk-container">
    <div class="uk-card uk-card-default uk-card-body uk-margin-large-bottom">
        <h1 class="uk-text-center">Registrar-se</h1>
        <h5>Os campos marcados com <?php echo $this->Html->getRequired() ?> são de preenchimento obrigatório.</h5>
        <hr>
        <form class="uk-form-stacked uk-margin-small" method="post" enctype="multipart/form-data">

            <?php
            # login
            echo $this->Html->getFormInput('Login', 'login', $Usuario->login, 'text', 'Nome de usuário', true);
            # matricula
            echo $this->Html->getFormInput('Matricula', 'matricula', $Usuario->matricula, 'text', 'Matrícula de professor', true);
            # nome
            echo $this->Html->getFormInput('Nome', 'nome', $Usuario->nome, 'text', 'Seu nome', true);
            # sobrenome
            echo $this->Html->getFormInput('Sobrenome', 'sobrenome', $Usuario->sobrenome, 'text', 'Seu sobrenome', true);
            # foto
            echo '<div class="uk-margin" uk-margin>
        <div class="uk-width-1-1 " uk-form-custom="target: true">
            <label for="foto">
                Escolha sua foto de perfil
            </label>
            <input type="file" id="foto" name="foto">
            <input class="uk-input" type="text" placeholder="Foto de perfil" disabled>
        </div>
    </div>';
            # email
            echo $this->Html->getFormInput('Email', 'email', $Usuario->email, 'text', 'E-mail', true);
            # senha
            echo $this->Html->getFormInput('Senha', 'senha', '', 'password', 'Senha', false);
            # nascimento
            echo $this->Html->getFormInput('Nascimento', 'nascimento', $Usuario->nascimento, 'date', 'Data de nascimento', true);
            # descricao
            echo $this->Html->getFormTextareaHtml('Descrição', 'descricao', $Usuario->descricao, 'Fale um pouco sobre você, sua área, especializações, etc...', true);
            if ($this->getParam('url_origem')) {
                echo $this->Html->getFormHidden('url_origem', $this->getParam('url_origem'));
            }
            ?>
            <div class="uk-text-right">
                <?php
                if ($this->getParam('url_origem')) {
                    $url_destino = Cript::decript($this->getParam('url_origem'));
                } else {
                    $url_destino = $this->Html->getUrl('Usuario', 'lista');
                }
                ?>
                <a href="<?php echo $this->Html->getUrl('Index', 'index') ?>" class="uk-button uk-button-danger uk-margin-small-bottom">Cancelar</a>
                <input type="submit" class="uk-button uk-button-primary uk-margin-small-bottom" value="salvar">
            </div>

        </form>
    </div>
</div>