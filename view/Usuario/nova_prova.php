<?php
/* @var $this UsuarioController */
?>
<?php
    function geraProva($questoes){
        $provinha = '';
        $provinha .= '<p>Instituição: ________________________________________________________________<br>';
        $provinha .= 'Aluno(a): _________________________________________________________________<br>';
        $provinha .= 'Professor: ________________________________________________________________<br>';
        $provinha .= 'Data: __/__/____<br>';
        $provinha .= 'Disciplina: ________________________________________________________________<br></p>';
        $provinha .= '<h2 class="uk-text-center">Prova de matemática</h2>';
        $provinha .= '<br>';
        $numero = 0;
        foreach($questoes as $q) {
            $numero++;
            $provinha .= '<p>'.$numero . ' - ' . strip_tags($q->texto, '<img><a><h4><h5><h6>'. '</p><br>');
            
            // Se for discursiva
            if(is_a($q, 'Discursiva')) {
                $linhas = (array) $q->getRespostas();
                for($i=0; $i<$linhas[0]->texto-1; $i++) {
                    $provinha .= '<br>______________________________________________________________________________________________________________________________';
                }
                continue;
            }
            // Se for objetiva
            if(is_a($q, 'Objetiva')) {  
                $ol = ['a', 'b', 'c', 'd', 'e'];
                $c = new Criteria();
                $c->setLimit(5);
                while(true) {
                    $alternativas = (array)$q->getRespostas($c);
                    shuffle($alternativas);
                    
                    $numero_alternativas = 0;
                    $possui_correta = false;
                    $respostas = '';

                    foreach($alternativas as $a) {
                        $respostas .= $ol[$numero_alternativas]. ') ' . $a->texto.'<br>';
                        $numero_alternativas++;
                        if($a->averigua) {
                            $possui_correta = true;
                        }
                    }
                    if($possui_correta) {
                        break;
                    }
                }
            }
            // Se for somatória
            if(is_a($q, 'Somatoria')) {
                $ol = ['01', '02', '04', '08', '16', '32', '64'];
                $c = new Criteria();
                $c->setLimit(7);
                while(true) {
                    $alternativas = (array)$q->getRespostas($c);
                    shuffle($alternativas);
                    
                    $numero_alternativas = 0;
                    $possui_correta = false;
                    $respostas = '';

                    foreach($alternativas as $a) {
                        $respostas .= $ol[$numero_alternativas]. ') ' . $a->texto.'<br>';
                        $numero_alternativas++;
                        if($a->averigua) {
                            $possui_correta = true;
                        }
                    }
                    if($possui_correta) {
                        break;
                    }
                }
            }
            $provinha .= '<p>'.$respostas . '</p><br>';
        }
        $provinha .= '<div class="page-break"></div>';
        return $provinha;
    }
    
?>
<style>

    @media print {
        @page {
            margin: 1cm;
        }
        p {
            widows: 3;
            orphans: 3;
        }

        .page-break { 
            page-break-before: always; 
        }

        .uk-navbar, #debugpanel, p[align="center"], .uk-iconnav {
            display: none;
        }

        .uk-margin-auto {
            margin: 0 !important;
        }
        .uk-container{
            padding: 0 !important;
        }
        .uk-padding{
            padding: 0 !important;
        }
    }

</style>
<div class="uk-container">
<ul class="uk-iconnav uk-background-secondary uk-iconnav-vertical uk-light uk-padding-small" style="text-align: center;position: fixed; top: 40%; right: 30px; width: 50px; height: 50px; border-radius: 100%;">
    <li><a href="javascript:window.print()" title="Imprimir" uk-icon="icon: print; ratio: 2"></a></li>
</ul>
    <div class="uk-background-default uk-margin-auto uk-padding pagina">

        <?php
        for($i = 0; $i < $quantidade_provas; $i++){
            echo geraProva($questoes);
        }
        ?>

    </div>
</div>