<?php
# Visão view/Usuario/ver.php 
/* @var $this UsuarioController */
/* @var $Usuario Usuario */
?>
<div class="uk-container">
    <div class="uk-margin-large-top uk-card uk-card-body uk-card uk-card-default" uk-grid>
        <!-- Foto -->
        <div class="uk-width-1-3">
            <img data-src="<?php echo SITE_PATH . '/' .$Usuario->foto?>" width="255" height="354" alt="Imagem de perfil" uk-img>
        </div>

        <!-- Informações -->
        <div class="uk-width-2-3 uk-child-width-1-1" uk-grid>
            <?php
            $data = date_format(date_create($Usuario->nascimento), 'd/m/Y');
            echo '<h3>' . $Usuario->nome . ' ' . $Usuario->sobrenome . '</h3>';
            echo '<p>' . $data . '</p>';
            echo '<p>' . $Usuario->email . '</p>';
            echo '<p>' . $Usuario->descricao . '</p>';
            ?>
            <div class="uk-text-right">
                <a href="<?php echo $this->Html->getUrl('Usuario', 'editar', array($Usuario->id_usuario))?>" class="uk-button uk-button-primary">Editar perfil</a>
            </div>
        </div>
    </div>
</div>