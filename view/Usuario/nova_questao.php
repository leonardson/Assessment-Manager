<?php
# Visão view/Usuario/cadastrar.php
/* @var $this UsuarioController */
/* @var $Usuario Usuario */
?>
<div class="uk-container">
    <div class="uk-card uk-card-default uk-card-hover uk-card-body uk-margin-large-top uk-margin-large-bottom">
        <h1 class="uk-text-center">Registrar nova questão</h1>
        <hr>
        <h4 class="uk-text-center">Selecione o tipo de questão</h4>
        <div class="uk-child-width-1-3 uk-margin-auto" uk-grid>
            <button class="uk-button uk-button-primary" id="discursiva">Discursiva</button>
            <button class="uk-button uk-button-primary" id="somatoria">Somatória</button>
            <button class="uk-button uk-button-primary" id="objetiva">Objetiva</button>
        </div>
        <div class="uk-margin" id="formulario"></div>
    </div>
</div>

<script>
$(document).ready(function() {
    $('#discursiva').click(function() {
        $('#formulario').html('<span class="uk-margin-top">Carregando...</span>');
        $.ajax({
            url: "<?php echo $this->Html->getUrl('Discursiva', 'cadastrar', array('template' => 'off')); ?>",
            success: function(data) {
                $('#formulario').html(data); 
            }
        });
    });
    $('#somatoria').click(function() {
        $('#formulario').html('<span class="uk-margin-top">Carregando...</span>');
        $.ajax({
            url: "<?php echo $this->Html->getUrl('Somatoria', 'cadastrar', array('template' => 'off')); ?>",
            success: function(data) { 
                $('#formulario').html(data); 
            }
        });
    });
    $('#objetiva').click(function() {
        $('#formulario').html('<span class="uk-margin-top">Carregando...</span>');
        $.ajax({
            url: "<?php echo $this->Html->getUrl('Objetiva', 'cadastrar', array('template' =>'off')); ?>",
            success: function(data) { 
                $('#formulario').html(data); 
            }
        });
    });
})
</script>