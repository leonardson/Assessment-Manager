<div class="uk-container">
    <div class="uk-card uk-card-default uk-card-body ">
        <div class="uk-panel">
            <h1 class="uk-align-left">Banco de questões</h1>
            <a href="<?php echo $this->Html->getUrl('Usuario', 'nova_questao') ?>" class="uk-button uk-button-primary uk-align-right uk-margin-small-top">Nova questão</a>
        </div>
        <hr>
        <form action="<?php echo $this->Html->getUrl('Usuario', 'banco',array(Session::get('user')->id_usuario)); ?>">
            <div class="uk-margin uk-inline uk-width-1-1">
                <span class="uk-form-icon uk-form-icon-flip" uk-icon="icon: search"></span>
                <input class="uk-input uk-width-1-1" id="pesquisa" onkeyup="filterBanco()" name="pesquisa" type="text" placeholder="Pesquisa tag">
            </div>
        </form>
        <form action="<?php echo $this->Html->getUrl('Usuario', 'nova_prova'); ?>" method="post">
            <table id="questoes" class="uk-table uk-table-striped uk-table-middle uk-margin-large-bottom">
                <thead>
                    <tr>
                        <th class="uk-table-shrink">#</th>
                        <th class="uk-expand">Pergunta</th>
                        <th class="uk-width-small">Tags</th>
                        <th class="uk-width-small">Ver</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    foreach ($Somatorias as $s) {
                        switch($s->dificuldade) {
                            case 1:
                                $background_cor = '#edfbf6';
                                $cor = '#32d296';
                                break;
                            case 2:
                                $background_cor = '#fff6ee';
                                $cor = '#faa05a';
                                break;
                            case 3:
                                $background_cor = '#fef4f6';
                                $cor = '#f0506e';
                                break;
                        }
                        echo '<tr style="background-color: '.$background_cor.'; color: '.$cor.'">';
                        echo '<td><input class="uk-checkbox" name="questao[]" value="s-'.$s->id_Somatoria.'" type="checkbox"></td>';
                        echo '<td>' . $s->texto . '</td>';
                        echo '<td>';
                        $i = 0;
                        foreach($s->getTag_perguntaTags() as $t) {
                            echo $t->tag;
                            $i++;
                            $quantidade = count($s->getTag_perguntaTags());
                            if($quantidade != $i) {
                                echo ', ';
                            } else {
                                echo ' ';
                            }
                        }
                        echo '</td>';
                        echo '<td>'.$this->Html->getLink('Ver', 'Somatoria', 'ver', array($s->id_Somatoria)).'</td>';
                        echo '</tr>';
                    }
                    foreach ($Discursivas as $d) {
                        switch($d->dificuldade) {
                            case 1:
                                $background_cor = '#edfbf6';
                                $cor = '#32d296';
                                break;
                            case 2:
                                $background_cor = '#fff6ee';
                                $cor = '#faa05a;';
                                break;
                            case 3:
                                $background_cor = '#fef4f6';
                                $cor = '#f0506e';
                                break;
                        }
                        echo '<tr style="background-color: '.$background_cor.'; color: '.$cor.'">';
                        echo '<td ><input class="uk-checkbox" name="questao[]" value="d-'.$d->id_Discursiva.'" type="checkbox"></td>';
                        echo '<td>' . $d->texto . '</td>';
                        echo '<td>';
                        $i = 0;
                        foreach($d->getTag_perguntaTags() as $t) {
                            echo $t->tag;
                            $i++;
                            $quantidade = count($d->getTag_perguntaTags());
                            if($quantidade != $i) {
                                echo ', ';
                            } else {
                                echo ' ';
                            }
                        }
                        echo '</td>';
                        echo '<td>'.$this->Html->getLink('Ver', 'Discursiva', 'ver', array($d->id_Discursiva)).'</td>';
                        echo '</tr>';
                    }
                    foreach ($Objetivas as $o) {
                        switch($o->dificuldade) {
                            case 1:
                                $background_cor = '#edfbf6';
                                $cor = '#32d296';
                                break;
                            case 2:
                                $background_cor = '#fff6ee';
                                $cor = '#faa05a';
                                break;
                            case 3:
                                $background_cor = '#fef4f6';
                                $cor = '#f0506e';
                                break;
                        }
                        echo '<tr style="background-color: '.$background_cor.'; color: '.$cor.'">';
                        echo '<td><input class="uk-checkbox" name="questao[]" value="o-'.$o->id_Objetiva.'" type="checkbox"></td>';
                        echo '<td>' . $o->texto . '</td>';
                        echo '<td>';
                        $i = 0;
                        foreach($o->getTag_perguntaTags() as $t) {
                            echo $t->tag;
                            $i++;
                            $quantidade = count($o->getTag_perguntaTags());
                            if($quantidade != $i) {
                                echo ', ';
                            } else {
                                echo ' ';
                            }
                        }
                        echo '</td>';
                        echo '<td>'.$this->Html->getLink('Ver', 'Objetiva', 'ver', array($o->id_Objetiva)).'</td>';
                        echo '</tr>';
                    }
                    ?>
                </tbody>
            </table>
            <div class="uk-position-fixed uk-background-secondary uk-padding uk-hover uk-light" style="bottom: 0; right: 0;">
                <div class="uk-margin">
                    <input class="uk-input" type="number" step="1" name="quantidade_provas" placeholder="Número de provas">
                </div>
                <input type="submit" class="uk-button uk-button-primary uk-width-1-1 " value="Gerar provas">
            </div>
        </form>
    </div>
</div>

<script>
    
    function filterBanco() {
        // Declare variables 
        var input, filter, table, tr, td, i, txtValue;
        input = document.getElementById("pesquisa");
        filter = input.value.toUpperCase();
        table = document.getElementById("questoes");
        tr = table.getElementsByTagName("tr");

        // Loop through all table rows, and hide those who don't match the search query
        for (i = 0; i < tr.length; i++) {
            td = tr[i].getElementsByTagName("td")[2];
            if (td) {
                txtValue = td.textContent || td.innerText;
                if (txtValue.toUpperCase().indexOf(filter) > -1) {
                    tr[i].style.display = "";
                } else {
                    tr[i].style.display = "none";
                }
            } 
        }
    }

</script>