<?php
# Visão view/Usuario/apagar.php 
/* @var $this UsuarioController */
/* @var $Usuario Usuario */
?>
<form class="form" method="post" action="<?php echo $this->Html->getUrl('Usuario', 'apagar') ?>">
    <h1>Confirmação</h1>
    <div class="well well-lg">
        <p>Voce tem certeza que deseja excluir o Usuario <strong><?php echo $Usuario->matricula; ?></strong>?</p>
    </div>
    <div class="text-right">
        <input type="hidden" name="id" value="<?php echo $Usuario->id_usuario; ?>">
        <a href="<?php echo $this->Html->getUrl('Usuario', 'lista') ?>" class="btn btn-default" data-dismiss="modal">Cancelar</a>
        <input type="submit" class="btn btn-danger" value="Excluir">
    </div>
    <?php
        echo $this->Html->getFormHidden('url_origem', $this->getParam('url_origem'));
    ?>
</form>
<!-- LazyPHP.com.br -->