<?php
# Visão view/Somatoria/cadastrar.php
/* @var $this SomatoriaController */
/* @var $Somatoria Somatoria */
?>
<script>
    var i = 0;
    var array_valor = [];
    function toggle(id) {
        if( array_valor[id].isCorrect ){
            array_valor[id].isCorrect = false;
        } else {
            array_valor[id].isCorrect = true;
        }
    }
</script>
<div class="uk-container">
    <div class="uk-panel uk-margin-large-top uk-margin-large-bottom">
        <h1 class="uk-text-center">Registrar uma pergunta de <b>somatória</b></h1>
        <h5>Os campos marcados com <?php echo $this->Html->getRequired() ?> são de preenchimento obrigatório.</h5>
        <hr>
        <form class="uk-form-stacked uk-margin-small" action="<?php echo $this->Html->getUrl('Somatoria', 'cadastrar') ?>" method="post">
            <?php
            # texto
            echo $this->Html->getFormTextareaHtml('Enunciado ', 'texto', $Somatoria->texto, 'Escreva o enunciado da pergunta', true);
            # dificuldade
            echo '<div class="uk-margin">';
            echo '<label for="dificuldade">Escolha a dificuldade da questão</label>';
            echo '<select class="uk-select" name="dificuldade" id="dificuldade">';
            echo '<option value="1">Fácil</option>';
            echo '<option value="2">Intermdiário</option>';
            echo '<option value="3">Difícil</option>';
            echo '</select>';
            echo '</div>';
            #tags
            echo $this->Html->getFormInput('Tags', 'tags', '', 'text', 'Ex: #matematica; #geometria; #equacao...', true);
            if ($this->getParam('url_origem')) {
                echo $this->Html->getFormHidden('url_origem', $this->getParam('url_origem'));
            }
            ?>
            <div id="alternativas">
                <h6>Alternativas <?php echo $this->Html->getRequired() ?></h6>
                <div class="uk-margin altern">
                    <div class="uk-inline uk-width-1-1">
                        <a class="uk-form-icon uk-form-icon-flip" id="add-alternativa" uk-icon="icon: plus"></a>
                        <input class="uk-input" id="field-alternativa" placeholder="Digite uma alternativa" type="text">
                    </div>
                </div>
                <ul class="uk-list uk-list-divider" id="alternas"></ul>
                <input type="hidden" name="alternativas" id="input-alternativa">
            </div>
            <hr>
            <?php
            if ($this->getParam('url_origem')) {
                echo $this->Html->getFormHidden('url_origem', $this->getParam('url_origem'));
            }
            ?>
            <div class="uk-text-right">
                <?php
                if ($this->getParam('url_origem')) {
                    $url_destino = Cript::decript($this->getParam('url_origem'));
                } else {
                    $url_destino = $this->Html->getUrl('Usuario', 'lista');
                }
                ?>
                <a href="<?php echo $this->Html->getUrl('Usuario', 'nova_questão') ?>" class="uk-button uk-button-danger uk-margin-small-bottom">Cancelar</a>
                <input type="submit" class="uk-button uk-button-primary uk-margin-small-bottom" value="salvar">
            </div>

        </form>
    </div>
</div>
<script>
    $('document').ready(function () {
        $('#add-alternativa').click(function () {
            if ($('#field-alternativa').val() != '') {
                var valor = $('#input-alternativa').val();
                var alternativa = {id: i, isCorrect: false, valor: $('#field-alternativa').val()};
                array_valor.push(alternativa);
                $('#input-alternativa').val(array_valor);
                $('#alternas').append('<li><input type="checkbox" onchange="toggle(' + i + ')" class="uk-checkbox" id="' + i + '"> ' + $('#field-alternativa').val() + '</li>');
                $('#field-alternativa').val('');
                i++;
            }
        });
        $('#field-alternativa').keypress(function (e) {
            if (e.which == 13) {
                if ($('#field-alternativa').val() != '') {
                    var valor = $('#input-alternativa').val();
                    var alternativa = {id: i, isCorrect: false, valor: $('#field-alternativa').val()};
                    array_valor.push(alternativa);
                    $('#input-alternativa').val(array_valor);
                    $('#alternas').append('<li><input type="checkbox" onchange="toggle(' + i + ')" class="uk-checkbox" id="' + i + '"> ' + $('#field-alternativa').val() + '</li>');
                    $('#field-alternativa').val('');
                    i++;
                }
            }
        });
        $(document).on("keypress", 'form', function (e) {
            var code = e.keyCode || e.which;
            if (code == 13) {
                e.preventDefault();
                return false;
            }
        });
        $('form').submit(function () {
            $('#input-alternativa').val(JSON.stringify(array_valor));
        })
    });
</script>