<?php
# Visão view/Somatoria/editar.php
/* @var $this SomatoriaController */
/* @var $Somatoria Somatoria */
?>
<div class="Somatoria editar panel panel-default">
    <div class="panel-heading">
        <h1>Editar Somatoria</h1>
    </div>
    <div class="panel-body">
    <form class="form-horizontal" method="post" role="form" action="<?php echo $this->Html->getUrl('Somatoria', 'editar', array($Somatoria->id_Somatoria)) ?>"  enctype="multipart/form-data">
    <div class="text-info">Os campos marcados com <i class="fa fa-asterisk"></i> são de preenchimento obrigatório.</div>
    <br>
        <?php
            echo $this->Html->getFormHidden('id_Somatoria', $Somatoria->id_Somatoria);
        # texto
        if($this->getParam('texto')){
            echo $this->Html->getFormHidden('texto', $this->getParam('texto'));
        } else {
            echo $this->Html->getFormTextareaHtml('Texto', 'texto', $Somatoria->texto, '', false);
        }
        # checking
        if($this->getParam('checking')){
            echo $this->Html->getFormHidden('checking', $this->getParam('checking'));
        } else {
            echo $this->Html->getFormInputCheckbox('Checking', 'checking', $Somatoria->checking);
        }
        # dificuldade
        if($this->getParam('dificuldade')){
            echo $this->Html->getFormHidden('dificuldade', $this->getParam('dificuldade'));
        } else {
            echo $this->Html->getFormInputCheckbox('Dificuldade', 'dificuldade', $Somatoria->dificuldade);
        }
        # id_usuario
        if ($this->getParam('id_usuario')) {
            echo $this->Html->getFormHidden('id_usuario', $this->getParam('id_usuario'));
        } else {
            echo $this->Html->getFormSelect('Usuario', 'id_usuario', array_columns((array) $Usuarios,'matricula', 'id_usuario'));
        }
        if($this->getParam('url_origem')){
            echo $this->Html->getFormHidden('url_origem', $this->getParam('url_origem'));
        }
        ?>
        <div class="clearfix"></div>
        <div class="text-right">
        <?php
        if($this->getParam('url_origem')){
            $url_destino = Cript::decript($this->getParam('url_origem'));
        } else {
            $url_destino = $this->Html->getUrl('Somatoria', 'lista');
        } ?>
            <a href="<?php echo $url_destino ?>" class="btn btn-default" data-dismiss="modal">Cancelar</a>
            <input type="submit" class="btn btn-primary" value="salvar">
        </div>
    </form>
    </div> <!-- .panel-body -->
</div> <!-- .panel -->
<!-- LazyPHP.com.br -->