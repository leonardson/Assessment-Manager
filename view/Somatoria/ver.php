<?php
# Visão view/Somatoria/ver.php 
/* @var $this SomatoriaController */
/* @var $Somatoria Somatoria */
?>
<div class="ver somatoria uk-container">
    <div class="uk-card uk-card-body uk-card-default uk-padding">
        <h1 class="uk-card-title"><?php echo $Somatoria->texto;?></h1>
        <p>Lembre-se, a ordem das questões será aleatória na hora de gerar a prova</p>
        <h4>Alternativas</h4>
        <ul>
            <?php
            foreach($Somatoria->getRespostas() as $resposta) {
                $certa = '';
                if($resposta->averigua) {
                    $certa = 'checked';
                }
                echo '<li><input class="uk-checkbox uk-margin-right" type="checkbox" '.$certa.' disabled>'.$resposta->texto.'</li>';
            }
            ?>
        </ul>
        <hr>
        <div class="uk-text-right">
            <?php echo $this->Html->getModalLink('Apagar', 'Somatoria', 'apagar', array($Somatoria->id_Somatoria), array('class' => 'uk-button uk-button-danger modal-trigger'))?>
        </div>
        <div class="clearfix"></div>
    </div>
</div>
<!-- LazyPHP.com.br -->