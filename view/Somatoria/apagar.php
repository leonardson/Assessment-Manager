<?php
# Visão view/Somatoria/apagar.php 
/* @var $this SomatoriaController */
/* @var $Somatoria Somatoria */
?>
<form class="form" method="post" action="<?php echo $this->Html->getUrl('Somatoria', 'apagar') ?>">
    <h1 class="uk-modal-title">Confirmação</h1>
    <p>Você tem certeza que deseja apagar esta questão de somatória?</p>
    <div class="uk-text-right">
        <input type="hidden" name="id" value="<?php echo $Somatoria->id_Somatoria; ?>">
        <a href="#" class="uk-button uk-button-default uk-modal-close">Cancelar</a>
        <input type="submit" class="uk-button uk-button-danger" value="Excluir">
    </div>
    <?php
        echo $this->Html->getFormHidden('url_origem', $this->getParam('url_origem'));
    ?>
</form>
<!-- LazyPHP.com.br -->