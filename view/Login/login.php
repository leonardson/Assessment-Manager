<div class="uk-width-large uk-position-center" style="width:60vh; ">
    <div class="uk-card uk-card-default uk-card-hover uk-card-body ">
        <h1 class="uk-text-center">Entrar</h1>
        <form class="form" method="post">
            <div class="uk-margin">
                <div class="uk-inline uk-width-1-1">
                    <span class="uk-form-icon" uk-icon="icon: user"></span>
                    <input type="text" name="login" placeholder="Login" class="uk-input">
                </div>
            </div>
            <div class="uk-margin">
                <div class="uk-inline uk-width-1-1">
                    <span class="uk-form-icon" uk-icon="icon: lock"></span>
                    <input type="password" name="password" class="uk-input" placeholder="Senha">
                </div>
            </div>
            <button class="uk-button uk-button-primary uk-width-1-1 uk-margin-small-bottom">Entrar</button>
            <div class="uk-child-width-1-2" uk-grid>
                <span>
                    <?php echo $this->Html->getLink('Registrar-se', $user, 'cadastrar'); ?>
                </span>
                <span>
                    <?php echo $this->Html->getLink('Esqueceu sua senha?', 'Login', 'send'); ?>
                </span>
            </div>
        </form>
    </div>
</div>