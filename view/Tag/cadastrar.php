<?php
# Visão view/Tag/cadastrar.php
/* @var $this TagController */
/* @var $Tag Tag */
?>
<div class="Tag cadastrar panel panel-default">
    <div class="panel-heading">
        <h1>Cadastrar Tag</h1>
    </div>
    <div class="panel-body">
    <form class="form-horizontal" method="post" role="form" action="<?php echo $this->Html->getUrl('Tag', 'cadastrar') ?>"  enctype="multipart/form-data">
    <div class="text-info">Os campos marcados com <i class="fa fa-asterisk"></i> são de preenchimento obrigatório.</div>
    <br>
        <?php
        # tag
        echo $this->Html->getFormInput('Tag', 'tag', $Tag->tag, 'password', '', false);
        # id_TagQuestao
        if ($this->getParam('id_TagQuestao')) {
            echo $this->Html->getFormHidden('id_TagQuestao', $this->getParam('id_TagQuestao'));
        } else {
            echo $this->Html->getFormSelect('Questao', 'id_TagQuestao', array_columns((array) $Questaos,'', 'id_Questao'));
        }
        if($this->getParam('url_origem')){
            echo $this->Html->getFormHidden('url_origem', $this->getParam('url_origem'));
        }
        ?>
        <div class="clearfix"></div>
        <div class="text-right">
        <?php
        if($this->getParam('url_origem')){
            $url_destino = Cript::decript($this->getParam('url_origem'));
        } else {
            $url_destino = $this->Html->getUrl('Tag', 'lista');
        } ?>
            <a href="<?php echo $url_destino ?>" class="btn btn-default" data-dismiss="modal">Cancelar</a>
            <input type="submit" class="btn btn-primary" value="salvar">
        </div>
    </form>
    </div> <!-- .panel-body -->
</div> <!-- .panel -->
<!-- LazyPHP.com.br -->