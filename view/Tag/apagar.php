<?php
# Visão view/Tag/apagar.php 
/* @var $this TagController */
/* @var $Tag Tag */
?>
<form class="form" method="post" action="<?php echo $this->Html->getUrl('Tag', 'apagar') ?>">
    <h1>Confirmação</h1>
    <div class="well well-lg">
        <p>Voce tem certeza que deseja excluir o Tag <strong><?php echo $Tag->tag; ?></strong>?</p>
    </div>
    <div class="text-right">
        <input type="hidden" name="id" value="<?php echo $Tag->id_Tag; ?>">
        <a href="<?php echo $this->Html->getUrl('Tag', 'lista') ?>" class="btn btn-default" data-dismiss="modal">Cancelar</a>
        <input type="submit" class="btn btn-danger" value="Excluir">
    </div>
    <?php
        echo $this->Html->getFormHidden('url_origem', $this->getParam('url_origem'));
    ?>
</form>
<!-- LazyPHP.com.br -->