<?php
# Visão view/Tag/ver.php 
/* @var $this TagController */
/* @var $Tag Tag */
?>
<div class="ver tag panel panel-default">
<div class="panel-body">
    <div class="panel panel-info">
        <div class="panel-heading">
            <h1><?php echo $Tag->tag;?></h1>
        </div>
        <div class="panel-body">
            <div class="atributo col-sm-6">
                <div class="col-md-3"><strong>Questao</strong>: </div>
                <div class="col-md-9">
                    <?php
                    echo $this->Html->getLink($Tag->getQuestao()->, 'Questao', 'ver',
                        array($Tag->getQuestao()->), // variaveis via GET opcionais
                        array('class' => '')); // atributos HTML opcionais
                ?>
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
    <br>
    

    
</div>
</div>
<!-- LazyPHP.com.br -->