<?php
# Visão view/Tag_pergunta/editar.php
/* @var $this Tag_perguntaController */
/* @var $Tag_pergunta Tag_pergunta */
?>
<div class="Tag_pergunta editar panel panel-default">
    <div class="panel-heading">
        <h1>Editar Tag_pergunta</h1>
    </div>
    <div class="panel-body">
    <form class="form-horizontal" method="post" role="form" action="<?php echo $this->Html->getUrl('Tag_pergunta', 'editar', array($Tag_pergunta->id_Tag_Pergunta)) ?>"  enctype="multipart/form-data">
    <div class="text-info">Os campos marcados com <i class="fa fa-asterisk"></i> são de preenchimento obrigatório.</div>
    <br>
        <?php
            echo $this->Html->getFormHidden('id_Tag_Pergunta', $Tag_pergunta->id_Tag_Pergunta);
        # id_Tag
        if ($this->getParam('id_Tag')) {
            echo $this->Html->getFormHidden('id_Tag', $this->getParam('id_Tag'));
        } else {
            echo $this->Html->getFormSelect('Tag', 'id_Tag', array_columns((array) $Tags,'tag', 'id_Tag'));
        }
        # id_Discursiva
        if ($this->getParam('id_Discursiva')) {
            echo $this->Html->getFormHidden('id_Discursiva', $this->getParam('id_Discursiva'));
        } else {
            echo $this->Html->getFormSelect('Discursiva', 'id_Discursiva', array_columns((array) $Discursivas,'id_Discursiva', 'id_Discursiva'));
        }
        # id_Objetiva
        if ($this->getParam('id_Objetiva')) {
            echo $this->Html->getFormHidden('id_Objetiva', $this->getParam('id_Objetiva'));
        } else {
            echo $this->Html->getFormSelect('Objetiva', 'id_Objetiva', array_columns((array) $Objetivas,'id_Objetiva', 'id_Objetiva'));
        }
        # id_Somatoria
        if ($this->getParam('id_Somatoria')) {
            echo $this->Html->getFormHidden('id_Somatoria', $this->getParam('id_Somatoria'));
        } else {
            echo $this->Html->getFormSelect('Somatoria', 'id_Somatoria', array_columns((array) $Somatorias,'id_Somatoria', 'id_Somatoria'));
        }
        if($this->getParam('url_origem')){
            echo $this->Html->getFormHidden('url_origem', $this->getParam('url_origem'));
        }
        ?>
        <div class="clearfix"></div>
        <div class="text-right">
        <?php
        if($this->getParam('url_origem')){
            $url_destino = Cript::decript($this->getParam('url_origem'));
        } else {
            $url_destino = $this->Html->getUrl('Tag_pergunta', 'lista');
        } ?>
            <a href="<?php echo $url_destino ?>" class="btn btn-default" data-dismiss="modal">Cancelar</a>
            <input type="submit" class="btn btn-primary" value="salvar">
        </div>
    </form>
    </div> <!-- .panel-body -->
</div> <!-- .panel -->
<!-- LazyPHP.com.br -->