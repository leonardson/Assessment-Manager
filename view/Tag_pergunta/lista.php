<?php
# Visão view/Tag_pergunta/lista.php 
/* @var $this Tag_perguntaController */
/* @var $Tag_perguntas Tag_pergunta[] */
?>
<div class="Tag_pergunta lista panel panel-default">
    <!-- titulo da pagina -->
    <div class="panel-heading">
        <h1>Tag_perguntas</h1>
    </div>
        

    <div class="panel-body">
        <!-- botao de cadastro -->
        <div class="text-right pull-right">
            <p><?php echo $this->Html->getLink('<i class="fa fa-plus-circle"></i> Cadastrar Tag_pergunta', 'Tag_pergunta', 'cadastrar', NULL, array('class' => 'btn btn-primary')); ?></p>
        </div>

        <!-- formulario de pesquisa -->
        <div class="pull-left">
            <form class="form-inline" role="form" method="get" action="<?php echo $this->Html->getUrl(CONTROLLER,ACTION,array('ordenaPor'=>$this->getParam('ordenaPor')))?>">
                <div class="form-group">
                    <label class="sr-only" for="pesquisa">Pesquisar</label>
                    <input value="<?php echo $this->getParam('pesquisa') ?>" type="search" class="form-control" name="pesquisa" id="pesquisa" placeholder="Tag">
                </div>
                <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
            </form>
        </div>

        <div class="clearfix"></div>
        <br>
        <!-- tabela de resultados -->
        <div class="table-responsive">
            <table class="table table-hover">
                <thead>
                    <tr>
                        <th>
                            <a href='<?php echo $this->Html->getUrl('Tag_pergunta', 'lista', array('ordenaPor' => 'id_Tag', 'pesquisa' => $this->getParam('pesquisa') )); ?>'>
                                Tag
                            </a>
                        </th>
                        <th>
                            <a href='<?php echo $this->Html->getUrl('Tag_pergunta', 'lista', array('ordenaPor' => 'id_Discursiva', 'pesquisa' => $this->getParam('pesquisa') )); ?>'>
                                Discursiva
                            </a>
                        </th>
                        <th>
                            <a href='<?php echo $this->Html->getUrl('Tag_pergunta', 'lista', array('ordenaPor' => 'id_Objetiva', 'pesquisa' => $this->getParam('pesquisa') )); ?>'>
                                Objetiva
                            </a>
                        </th>
                        <th>
                            <a href='<?php echo $this->Html->getUrl('Tag_pergunta', 'lista', array('ordenaPor' => 'id_Somatoria', 'pesquisa' => $this->getParam('pesquisa') )); ?>'>
                                Somatoria
                            </a>
                        </th>
                        <th>&nbsp;</th>
                        <th>&nbsp;</th>
                    </tr>
                </thead>
                <?php
                foreach ($Tag_perguntas as $t) {
                    echo '<tr>';
                        echo '<td>';
                        echo $this->Html->getLink($t->getTag()->tag, 'Tag', 'ver',
                            array($t->getTag()->id_Tag), // variaveis via GET opcionais
                            array()); // atributos HTML opcionais
                        echo '</td>';
                        echo '<td>';
                        echo $this->Html->getLink($t->getDiscursiva()->texto, 'Discursiva', 'ver',
                            array($t->getDiscursiva()->id_Discursiva), // variaveis via GET opcionais
                            array()); // atributos HTML opcionais
                        echo '</td>';
                        echo '<td>';
                        echo $this->Html->getLink($t->getObjetiva()->texto, 'Objetiva', 'ver',
                            array($t->getObjetiva()->id_Objetiva), // variaveis via GET opcionais
                            array()); // atributos HTML opcionais
                        echo '</td>';
                        echo '<td>';
                        echo $this->Html->getLink($t->getSomatoria()->texto, 'Somatoria', 'ver',
                            array($t->getSomatoria()->id_Somatoria), // variaveis via GET opcionais
                            array()); // atributos HTML opcionais
                        echo '</td>';
                        echo '<td width="50">';
                        echo $this->Html->getLink('<i class="fa fa-pencil-square-o"></i>', 'Tag_pergunta', 'editar', 
                            array($t->id_Tag_Pergunta), 
                            array('class' => 'text-warning', 'title' => 'editar'));
                        echo '</td>';
                        echo '<td width="50">';
                        echo $this->Html->getModalLink('<i class="fa fa-trash-o"></i>', 'Tag_pergunta', 'apagar', 
                            array($t->id_Tag_Pergunta), 
                            array('class' => 'text-danger', 'title' => 'apagar'));
                        echo '</td>';
                    echo '</tr>';
                }
                ?>
            </table>

            <!-- menu de paginação -->
            <div style="text-align:center"><?php echo $Tag_perguntas->getNav(); ?></div>
        </div> <!-- .table-responsive -->
    </div> <!-- .panel-body -->
</div> <!-- .panel -->

<script>
    /* faz a pesquisa com ajax */
    $(document).ready(function() {
        $('#pesquisa').keyup(function() {
            var r = true;
            if (r) {
                r = false;
                $("div.table-responsive").load(
                <?php
                if (isset($_GET['ordenaPor']))
                    echo '"' . $this->Html->getUrl('Tag_pergunta', 'lista', array('ordenaPor' => $_GET['ordenaPor'])) . 'pesquisa:" + encodeURIComponent($("#pesquisa").val()) + " .table-responsive"';
                else
                    echo '"' . $this->Html->getUrl('Tag_pergunta', 'lista') . 'pesquisa:" + encodeURIComponent($("#pesquisa").val()) + " .table-responsive"';
                ?>
                 , function() {
                    r = true;
                });
            }
        });
    });
</script>
<!-- LazyPHP.com.br -->