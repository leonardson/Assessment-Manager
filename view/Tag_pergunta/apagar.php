<?php
# Visão view/Tag_pergunta/apagar.php 
/* @var $this Tag_perguntaController */
/* @var $Tag_pergunta Tag_pergunta */
?>
<form class="form" method="post" action="<?php echo $this->Html->getUrl('Tag_pergunta', 'apagar') ?>">
    <h1>Confirmação</h1>
    <div class="well well-lg">
        <p>Voce tem certeza que deseja excluir o Tag_pergunta <strong><?php echo $Tag_pergunta->id_Tag_Pergunta; ?></strong>?</p>
    </div>
    <div class="text-right">
        <input type="hidden" name="id" value="<?php echo $Tag_pergunta->id_Tag_Pergunta; ?>">
        <a href="<?php echo $this->Html->getUrl('Tag_pergunta', 'lista') ?>" class="btn btn-default" data-dismiss="modal">Cancelar</a>
        <input type="submit" class="btn btn-danger" value="Excluir">
    </div>
    <?php
        echo $this->Html->getFormHidden('url_origem', $this->getParam('url_origem'));
    ?>
</form>
<!-- LazyPHP.com.br -->