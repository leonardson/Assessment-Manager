<?php
# Visão view/Tag_pergunta/ver.php 
/* @var $this Tag_perguntaController */
/* @var $Tag_pergunta Tag_pergunta */
?>
<div class="ver tag_pergunta panel panel-default">
<div class="panel-body">
    <div class="panel panel-info">
        <div class="panel-heading">
            <h1><?php echo $Tag_pergunta->id_Tag;?></h1>
        </div>
        <div class="panel-body">
            <div class="atributo col-sm-6">
                <div class="col-md-3"><strong>Discursiva</strong>: </div>
                <div class="col-md-9">
                    <?php
                    echo $this->Html->getLink($Tag_pergunta->getDiscursiva()->texto, 'Discursiva', 'ver',
                        array($Tag_pergunta->getDiscursiva()->id_Discursiva), // variaveis via GET opcionais
                        array('class' => '')); // atributos HTML opcionais
                ?>
                </div>
            </div>
            <div class="atributo col-sm-6">
                <div class="col-md-3"><strong>Objetiva</strong>: </div>
                <div class="col-md-9">
                    <?php
                    echo $this->Html->getLink($Tag_pergunta->getObjetiva()->texto, 'Objetiva', 'ver',
                        array($Tag_pergunta->getObjetiva()->id_Objetiva), // variaveis via GET opcionais
                        array('class' => '')); // atributos HTML opcionais
                ?>
                </div>
            </div>
            <div class="atributo col-sm-6">
                <div class="col-md-3"><strong>Somatoria</strong>: </div>
                <div class="col-md-9">
                    <?php
                    echo $this->Html->getLink($Tag_pergunta->getSomatoria()->texto, 'Somatoria', 'ver',
                        array($Tag_pergunta->getSomatoria()->id_Somatoria), // variaveis via GET opcionais
                        array('class' => '')); // atributos HTML opcionais
                ?>
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
    <br>
    

    
</div>
</div>
<!-- LazyPHP.com.br -->