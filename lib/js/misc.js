prevent_ajax_view = true;

$(document).ready(function () {
    $('.modal-trigger').click(function (e) {
        if ($(this).data('href') != "") {
            $.ajax({
                url: $(this).data('href'),
                success: function (response) {
                    $('.uk-modal-dialog').html(response);
                }
            })
        }
    });
});

